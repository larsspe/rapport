\subsection{Analyse}

Simuleringsverktøyet består av en webapplikasjon som har tatt inspirasjon av
\gls{SPA}-tankegangen. Kommunikasjon og annen data som trengs fra API-et hentes
asynkront ved hjelp av jQuery. Kjernen
av web-applikasjonen er Vue. Dette verktøyet gjør det enkelt å oppdatere
applikasjonens utseende basert på datastrukturen i bakgrunnen.
Simuleringsalgoritmen i API-et henter defekttypene som er angitt av brukeren, og
returnerer en oversikt over hvordan endringer i vurderingen av disse
defekttypene påvirker produksjonen.  Det endelige brukergrensesnittet for
verktøyet har blitt lagt ved som vedlegg~\ref{appendix:simulation_overview}.

\subsubsection{Nettsidens grunnstruktur}

Nettsiden er bygd med Vue. Dette er et rammeverk som benyttes ved utvikling av
SPA-webapplikasjoner. Kjernen til Vue består av spesialbygde
JavaScript-objekter, eller komponenter, som igjen er koblet til
HTML-elementer.  Når dataen som er lagret i komponenten endres, vil Vue
selv håndtere hvordan HTML-strukturen oppdateres automatisk.  Vue har en
rekke forhåndsdefinerte HTML-tags som beskriver hvordan dataen i
JavaScript-objektet skal reflekteres i utseendet. Hver komponent er
selvstendige enheter og de må definere klare grensesnitt om hvordan data skal
sendes mellom hverandre.


Den overordnede strukturen i simuleringsverktøyet som er vist i
figur~\ref{figure:vue_structure} består av tre typer underkomponenter
\lstinline{dropdown_menu}, \lstinline{manual-simulate-picker},
\lstinline{simulate_results}, og en hovedkomponent som håndterer
kommunikasjonen mellom komponentene. Denne strukturen har gjort det enkelt å
gjenbruke komponenter, samt enkelt kontrollere hvilke komponenter som vises
når, og hvordan.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.7\textwidth]{vue_diagram.png}
    \caption{Overordnet Vue komponentstruktur~\label{figure:vue_structure}.}
\end{figure}



\subsubsection{Valg av parametre}
\label{section:valg_av_parametre}

For å gjennomføre simuleringen må brukeren oppgi start- og sluttidspunkt,
ønskede defekttyper og hvilken skanner dataen skal hentes fra. For å slippe å skrive
inn datoen for hånd har en klikkbar kalender blitt implementert.
Figur~\ref{figure:datetimepicker} viser jQuery modulen
jquery-datetimepicker, som ga oss et rent brukergrensesnitt for valg
av både dato og klokkeslett.

\begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{datetimepicker.png}
    \caption{Valg av dato og klokkeslett.}
    \label{figure:datetimepicker}
\end{figure}


Valg av defekttyper og skanner besto av å la bruker velge blant eksisterende
verdier. Denne datastrukturen passer bra med nedtrekkslister, men vi trengte i
tillegg en tabell over valgte alternativer. Selve nedtrekkslisten ble
implementert med jQuery modulen Select2. Denne modulen gjorde det enkelt å
legge inn alternativene basert på data direkte fra API-et, og la modulen
håndtere hvordan brukeren samhandler med dette. Tabellen av valgte alternativer
implementerte vi ved å la Vue håndtere en HTML-tabell basert på innholdet
i en intern Javascript liste. Listing~\ref{listing:select2_click_handler} viser
hvordan vi implementerte dette gjennom definisjonen av HTML-tabellen og
Javascript koden som krevdes for å oppdatere denne. Her benyttes Vue sin
\lstinline{v-for} attributt for å iterere over innholdet i \lstinline{rows}
listen.
 
\begin{lstlisting}[language=HTML]
<table>
    <template v-for="(row, i) in rows">
        <tr>
            <td>
                <button v-on:click="remove(i)">Remove</button>
            </td>
            <td>{{row}}</td>
        </tr>
    </template>
</table>
\end{lstlisting}
\begin{lstlisting}[language=Javascript, caption={Insetting av nytt valg i tabell over valgte alternativer.},
label={listing:select2_click_handler}, firstnumber=11]
// Store the `this` for Vue as it will be overriden by Select2s `this`
// inside the event handler
var external_this = this;
dropdown_element.on("select2:select", function (e) {
    external_this.add(e.params.data.id);
});
\end{lstlisting}

Nedtrekkslisten og tabellen over valgte alternativer ligger i en egen komponent
\lstinline{dropdown-element}. Ved å samle disse, kan innholdet enkelt gjenbrukes av både
defekttype- og scannerparameterene.  Listing~\ref{listing:component_reuse}
viser hvordan dette enkelt gjøres ved å benytte komponenten som en HTML tag,
med tilhørende attributter. 

\begin{lstlisting}[language=HTML, caption={Gjenbruk av komponenter.}, label={listing:component_reuse}]
<div id="vueApplication">
    <dropdown-menu
            dropdown_label="Scannere"
    ></dropdown-menu>
    <dropdown-menu
            dropdown_label="Typer / Subtyper"
    ></dropdown-menu>
</div>
\end{lstlisting}

\subsubsection{Simuleringsprosessen}

Når de tidligere nevnte parametrene har blitt definert kan selve simuleringen
gjennomføres.  Dette startes av web-applikasjonen gjennom API-endepunktet
\lstinline{/simulate_images}.

Når Argos sin skanner har gjenkjent alle defektene på en plate, vil den
kategorisere defektene ved å sammenligne antallet defekter av en type mot dens
forhåndsdefinerte terskelverdier. Målet med simuleringen er å dermed å finne ut
hvordan en endring av disse terskelverdiene vil påvirke produksjonen. Et eksempel
på dette er dersom den gamle terskelverdien var to, men det ønskes å se hvor mange 
flere plater som hadde blitt godkjent ved å øke til fire.

For å visualisere dette har vi valgt å representere fordelingen av antall
defekter (i dette tilfellet hakk) i et paretodiagram. Dette er et søylediagram
som beskriver feilsituasjoner på x-aksen, og antall forekomster på y-aksen.  På
toppen av søylene er et linjediagram som viser kumulativ opptelling av antall
feil.  Figur~\ref{figure:pareto_chart_example} viser en fordeling av
situasjonen over, der feil 1 inntraff fire ganger. Den kumulative fordelingen
(her i oransje), viser at dette tilsvarer omtrent 30\% av totale antall feil.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{pareto_chart_example.png}
    \caption{Paretodiagram for fordelingen av hakk~\label{figure:pareto_chart_example}.}
\end{figure}

Simuleringsalgoritmen i analyseplatformen vil generere et paretodiagram
for hver defekttype. Y-aksen angir antall plater som ble nedgradert spesifikt
av denne defekttypen, og x-aksen angir antall feil av defekttypen som forekom
på platene. I eksempelet over er det fire plater som hadde en forekomst av
defekttypen. Simulert produksjonsendring kan med dette regnes ut ved å summere
søylene mellom ny og gammel grense (x-aksen). I
figur~\ref{figure:pareto_chart_example} ville produksjonen økt med 8
plater dersom gammel grense er en og ny grense er fire (inklusiv).  \\

For å konvertere rådataen fra databasen til et format som kan vises gjennom et
paretodiagram må følgende steg utføres: 

\begin{enumerate}
    \itemsep -0.2em
    \item Finne plater som har angitte defekttyper som årsak for nedgradering.
    \item Filtrere bort plater med andre defekter som ellers ville ført til nedgradering.
    \item Telle antall defekter per defekttype for hver plate.
    \item Telle antall plater ved å gruppere defekttype og antall defekter.
\end{enumerate}

Siden brukeren potensielt ønsker å spørre om flere defekttyper om gangen, var det
viktig å tenke på ytelse i algoritmen.  De tre første stegene ble dermed implementert
gjennom en større spørring til SQL, mens steg fire ble gjort i Python. Her
benyttet vi oss av SQLAlchemy sin modulære struktur for å dele opp koden inn i
mindre, mer forståelige deler.

Listing~\ref{listing:worst_subq} viser hvordan steg 1 ble gjennomført ved å
skrive en \lstinline{subquery}, eller delspørring. Gjennom listen \lstinline{where_argosnames}
og filteret \lstinline{Defects.c.IsWorstDefect == 1} blir \lstinline{Defects}
tabellen filtrert slik at den bare inneholder plater som er aktuelle.

Steg en ble gjennomført ved å hente alle platene som har angitte defekttyper som
grunnlag for nedgradering. Dette ble gjort som en subquery (se
listing~\ref{listing:worst_subq}), som gjør det mulig å referere til denne i
både steg to og tre. 

\begin{lstlisting}[
    language=Python,
    caption={Subquery for filtrering av Defects tabell.},
    label=listing:worst_subq
]
worst_subq = (
    session.query(Defects.c.BoardID, Defects.c.ArgosName)
    .join(Boards)
    .filter(Boards.c.Time.between(start_time, end_time))
    .filter(Defects.c.IsWorstDefect == 1)
    .filter(Defects.c.ArgosName.in_(where_argosnames))
    .subquery()
)
\end{lstlisting}

Steg to ble implementert i en separat delspørring der ny og gammel klasse
defineres ved variablene i listing~\ref{listing:new_old_class}. Her hentes den
gamle verdien som den høyeste klassen av alle defekter for en plate. Den nye
klassen blir hentet på samme vis, men ser bort ifra defekttypen som nedgraderte
platen.  Til slutt vil delspørringen filtrere bort alle plater som har lik ny
og gammel klasse.

\begin{lstlisting}[language=Python, caption={Definisjon av gammel og ny klasse.}, label={listing:new_old_class}]
old_class = func.max(Defects.c.Class)
# Get the highest class that is not worst defect.
new_class = func.max(
    case(
        [(Defects.c.ArgosName != worst_subq.c.ArgosName, Defects.c.Class)], else_=0
    )
)
\end{lstlisting}



Når filtreringen i steg en og to har blitt gjennomført kan steg tre telle opp
antall defekter per defekttype på platene som gjenstår. Dette vil gi et
resultat tilsvarende tabell~\ref{table:step1-3_result}.  Steg fire kan deretter
telle opp antall \lstinline{plate id} for hver defekttype, samt hver nye og
gamle klasse. Det endelige resultatet av denne prosessen er vist i
tabell~\ref{table:step1-4_result}. For å plotte denne dataen i et paretodiagram
blir kolonnen \lstinline{antall defekter} representert av x-aksen, mens
\lstinline{antall plater} blir representert av y-aksen. Funksjonen som utgjør
steg en til fire kan i sin helhet sees i
vedlegg~\ref{appendix:full_query_pareto}.

\begin{table}[]
    \centering
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \rowcolor[HTML]{EFEFEF}
    Plate ID & Defekttype & Antall defekter & Gammel klasse & Ny klasse \\ \hline
    1       & Hull         & 5             & Reparer          & Godkjent          \\ \hline
    2       & Bark         & 3             & Reparer          & Godkjent          \\ \hline
    3       & Hull         & 5             & Reparer          & Godkjent          \\ \hline
    4       & Bark         & 3             & Reparer          & Godkjent          \\ \hline
    5       & Bark         & 3             & Reparer          & Godkjent          \\ \hline
    6       & Hull         & 5             & Vrak          & Reparer          \\ \hline
    \end{tabular}
    \caption{Foreløpig resultat etter de første tre stegene.}
    \label{table:step1-3_result}
\end{table}

\begin{table}[!ht]
    \centering
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \rowcolor[HTML]{EFEFEF}
    Defekttype & Antall defekter & Ny klasse & Gammel klasse & Antall plater \\ \hline
    Hull         & 5             & Godkjent          & Reparer          & 2            \\ \hline
    Bark         & 3             & Godkjent          & Reparer          & 3            \\ \hline
    Hull         & 5             & Reparer          & Vrak          & 1            \\ \hline
    \end{tabular}
    \caption{Endelig resultat etter prosessering.}
    \label{table:step1-4_result}
\end{table}


\subsubsection{Bestemme terskelverdier}

Når datagrunnlaget har blitt laget ved hjelp av simuleringsprosessen kan dataen
analyseres ved å angi hva den gamle terskelverdi var og hva ny verdien skal være.  De
eksisterende terskelverdiene ligger lagret i skannerenes Windows registre.
Lesingen av disse verdiene ble gjort gjennom en egen klasse
\lstinline{ArgosRegistry}. For å holde systemet fleksibelt kan klassen lese
registeret gjennom Windows \lstinline{.reg} filformatet. Klassen eksponerer
deretter et definert grensesnitt for å hente aktuelle terskelverdier basert på
defekttype.

Den nye terskelverdien kan bestemmes på to måter, enten manuelt eller ved et
automatisk forslag. Manuell spesifisering av tersklene gjøres ved at brukeren
får opp et ekstra panel som gir muligheten å spesifisere terskelverdier for
hver defekttype. Dette panelet er implementert med Vue-komponenten
\lstinline{manual-simulate-picker} som vist i figur \ref{figure:vue_structure}.

Det automatiske forslaget regnes ut etter datagrunnlaget har blitt definert.
Det automatiske forslaget forsøker å balansere hvor mange flere plater som blir
oppgradert mot hvor mange flere defekter som tillates. Dette har blitt
implementert ved å begynne på den gamle terskelverdien å regne ut et løpende
standardavvik av plater for hver x-verdi. Dersom dette standardavviket blir
større enn en gitt terskel, vil algoritmen foreslå siste x-verdi som ny
terskelverdi. Svakhetene ved denne implementasjonen er at den potensielt vil
overskyte dersom antall plater er relativt stabilt.  Den vil også raskt stoppe
både dersom forskjellen i antall plater er både positivt og negativt, da
standardavvik er et avstandsmål uten fortegn.  Den første svakheten har vi løst
ved å legge inn en maksgrense for hvor langt algoritmen vil gå, uavhengig av
standardavviket, på denne måten unngår vi at algoritmen foreslår en uendelig
grense. På bakgrunn av datasettet vi har hatt tilgang til, har vi bedømt at den
andre svakheten inntreffer såpass sjeldent at det ikke vil være hensiktsmessig å
mitigere dette.

\subsubsection{Presentasjon av resultat}

Når ny terskelverdi er valgt kan sluttresultatet av simuleringen presenteres.
I tillegg til å se numerisk resultat, er det også mulig å se det
i paretodiagrammet. Et utdrag av selve visningen i nettleseren kan sees i
figur~\ref{figure:simulation_result}. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{simulate_result.png}
    \caption{Simureringsresultat med paretodiagram.}
    \label{figure:simulation_result}
\end{figure}





\clearpage


%TODO: Flytt dette opp i design.
%Argos sin scanner vurderer plater i to faser. Den første fasen detekterer
%selve feilene på bildene, mens den andre fasen bestemmer hvordan feilene skal
%påvirke klassen til platen. Hver type feil blir delt inn i undertyper, som
%igjen kan bestå av flere konkrete feilklasser. Se tabell
%(\ref{table:feiltype_oppbygning}) for eksempel.

%\begin{table}[]
%\centering
%\begin{tabular}{|l|l|l|}
%\hline
%Type                  & Subtype                     & Klasse         \\ \hline
%\multirow{4}{*}{Hull} & \multirow{2}{*}{Stort hull} & StortSmaltHull \\ \cline{3-3} 
%                      &                             & StortBredtHull \\ \cline{2-3} 
%                      & \multirow{2}{*}{Lite hull}  & LiteHull       \\ \cline{3-3} 
%                      &                             &                \\ \hline
%\end{tabular}
%\caption{Sammenheng mellom Typer, subtyper og Klasser 
%\label{table:feiltype_oppbygning}}
%\end{table}
