Arkitekturen til systemet består i hovedtrekk av en sentral tjener som tar imot
forespørsler fra eksterne systemer, og henter data fra angitte datakilder.
Figur~\ref{fig:overall_architechture} viser hvordan den sentrale tjeneren er
delt i en lagdelt modell, med en modulbasert arkitektur for definisjon av
funksjonalitet. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{architechture.png}
    \caption{\label{fig:overall_architechture} Overordnet arkitektur.}
\end{figure} 

\subsection{Overordnet arkitektur}

Oppgaven fra Argos er delt i tre forskjellige hoveddeler: Rapportgenerering,
sanntidsdata og simuleringsverktøy. Vi innså raskt at det vil være lite
hensiktsmessig å lage disse verktøyene fra bunn. Dermed fokuserte vi tidlig på
å bruke eksisterende programvare for å utføre de mest kompliserte delene, samt
bygge en egen plattform som kan binde disse systemene sammen. Argos' ønske for
analyseplattformen var at den skulle fungere som et bindeledd mellom de
eksisterende databasene og verktøy som benytter seg av disse.  Dette gjorde at
systemet måtte ha en arkitektur som la opp til enkel utvidelse i fremtiden, spesielt
i form av nye eksterne verktøy.

Hovedmodellen i fellesplattformen baserte vi på en lagdelt modell.  En lagdelt
modell tilfredsstiller kravene til analyseplattformen på grunn av sine lave
koblinger og høye \gls{kohesjon}. I denne modellen kan hvert lag sømløst byttes
ut, så lenge grensesnittet forblir det samme. Denne fleksibiliteten legger også
opp til å enkelt introdusere ny funksjonalitet i hvert lag.  Fleksibilitet i
utrullingen var også en faktor som måtte tas i betraktning.  Lave koblinger og
høy kohesjon gir også en mer glidende overgang til distribuert arkitektur, der
mindre deler av systemet kan flyttes til separate prosesser eller tjenere
etter behov.  Dette styrker analyseplattformens mulighet til å kjøre i et
skybasert miljø.

\subsection{Lagene i arkitekturen}

Figur~\ref{fig:overall_architechture} viser hvordan fellesplattformen er delt inn i
fire hovedlag: API, modulkontroller, moduler og databasetilgang. Her skal vi gå
mer inn i hva disse lagene må innebære for å oppfylle kravene til plattformen.

\subsubsection{API}

API-laget håndterer hvordan eksterne tjenester skal strukturere spørringer til
resten av systemet. I valg av teknologi (\ref{section:teknologi_api_rammeverk})
ble det tidlig bestemt at grensesnittet til eksterne tjenere skulle være
REST-basert, da dette tilbyr størst fleksibilitet.

Grafana og Report Builder har forskjellige strukturer for hvordan de
spør etter data, og hva de forventer at formatet på returnert data skal være.
Grafana sender parametrene til sine spørringer gjennom JSON-objekter, og
forventer svar i samme format. Report Builder derimot, benytter seg
av \lstinline{x-www-form-urlencoded} enkoding\footnote{Eksempelvis
\lstinline{param1=a&param2=b} mot JSON sitt 
\lstinline{\{'param1':'a', 'param2:'b'}\}}, og forventer XML-strukturert data
som respons. Disse kravene gjør at API-laget må kunne tjene disse formatene
samtidig.

Siden dette laget håndterer alle forespørsler til eksterne tjenester har den
rollen som førstelinjeforsvar mot sikkerhetstrusler. API-laget håndterer både
innkommende og returnerte data, og har derfor en spesielt viktig rolle
i å validere dataen i begge retninger.

\subsubsection{Modulkontroller} 

Det vil være lite formålstjenelig å prøve å lage generisk funksjonalitet som
dekker alle behov, og det vil være umulig å lage all mulig ønsket
funksjonalitet initielt. Disse rammene gjorde at et smidig, fleksibelt
rammeverk som er enkelt å videreutvikle var nødvendig.

Av de fem mest brukte arkitekturmodellene for
programvare~\cite{oriley_software_patterns} lener de overnevnte kravene seg mot en
mikrokjernebasert modell. Denne type modell består av en kjerne med
grunnleggende funksjonalitet, og selvstendige komponenter som enkelt kan legges
til eller fjernes. Modellen er regnet som en smidig modell som gir gode vilkår for
utrulling, testing og ytelse.  Modellen er svakere dersom man ønsker å skalere
opp hele tjenesten, og den gjør utvikling noe mer kompleks avhengig av hvor
mange krav til kjerne man har~\cite{oriley_software_patterns}.

I analyseplattformen vil kjernen bestå av nødvendig logikk for å tolke
parametre som alle moduler vil trenge, oppslag av modul for å gi spørringen
videre til riktig sted og håndtering av datastrukturen som returneres. I
figur~\ref{fig:overall_architechture} utgjør API- og modulkontrollerlagene
kjernen, mens modulene er de selvstendige komponentene. Styrkene til modellen
gjør at modulene selvstendig blir enklere å utvikle, da alle blir kalt med de
et forhåndsdefinert sett med parametre. Det er også lett å velge ut nøyaktig
hvilken funksjonalitet som rulles ut gjennom å velge hvilke moduler som
inkluderes. Modellen åpner også for at moduler kan lastes fra forskjellige
steder, for eksempel innebygd i kjerneplattformen eller distribuert over
forskjellige systemer

Ved utvikling av en mikrokjernebasert modell er det viktig å tidlig definere
hvordan grensesnittet mellom kjernen og modulene skal
være~\cite{oriley_software_patterns}. Etter undersøkelser innen
rapportgenerering og dashbord har vi kommet frem til at alle moduler vil trenge
følgende parametre:

\begin{itemize}
    \item \textbf{Datakilde}: Det vil være nødvendig for alle plugins å
    vite hvor de skal hente data fra.
    \item \textbf{Scannernavn}: For å kunne navngi den resulterende dataen
    trengs scannernavnet som sesjonen hører til. 
    \item \textbf{Tidspunkter}: Alle data hører til innenfor et gitt tidsintervall.
    \item \textbf{Annen data}: Hver plugin kan ha behov for ytterligere data
    enn de andre oppgitte punktene. Derfor må det også være mulig å tilby dette.
\end{itemize}

Dataflyten gjennom kjernen er vist i figur~\ref{fig:pluginmanager_dataflow}.


\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{pluginmanager_sequence_diagram.png}
    \caption{\label{fig:pluginmanager_dataflow} Sekvensdiagram for kjernen.
    } 
\end{figure} 

\subsubsection{Databasetilgang}

Det nederste laget i figur~\ref{fig:overall_architechture} er
datatilgangslaget. Et datatilgangslag bør inneholde funksjonalitet for
tilkobling til databaser, sesjonshåndtering, \gls{CRUD}-operasjoner og
tjenestefrihet~\cite{daoimportance}. Ved at datatilgangslaget tar seg av dette
vil modulene bare trenge å forholde seg til et predefinert grensesnitt, uten at
de trenger å vite hvor dataen kommer fra selv.

I kapittelet om teknologi ble det bestemt at systemet skulle bruke SQLAlchemy
for å gjøre spørringer mot databaser. SQLAlchemy kunne ivareta alle de
overnevnte kravene til et databasetilgangslag, men hovedsakelig bare for
individuelle tilkoblinger. Databasetilgangslaget i analyseplattformen består
dermed av et rammeverk som utvider SQLAlchemy sin funksjonalitet gjennom å
håndtere hvordan spørringene kjøres mot flere mulige databaser.
