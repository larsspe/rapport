I dette kapittelet går vi gjennom hvordan vi planla å gjennomføre utviklingsarbeidet. Vi
beskriver hvilke avveininger vi gjorde ved valg av utviklingsmodell, hvilke verktøy
vi benyttet og en risikoanalyse av prosjektperioden.

\subsection{Systemutviklingsmodell}
For å planlegge og strukturere arbeidet, er det i et hvert utviklingsprosjekt
essensielt å etablere en systemutviklingsmodell~\cite{soft_fuzzy}. Det
eksisterer mange typer modeller, som er tilpasset prosjekter med ulike
omfang, struktur og omstendigheter. Det vanskelige blir derfor å
velge modellen, eller de kombinasjonene av modeller, som passer best for vårt
prosjekt. For å ta en best mulig beslutning, startet vi med å avdekke de
faktorene i vårt prosjekt vi anser som de mest styrende for valg av
utviklingsmodell:

\begin{itemize}
	\item Gruppen består av tre utviklere og prosjektet skal gjennomføres på
	en relativt kort tidsperiode.
	\item Gruppen har lite erfaring med prosjekter av denne størrelsen.
	\item Utover de funksjonelle kravene i oppgavebeskrivelsen (vedlegg
	\ref{appendix:oppgavebeskrivelse}), legges det ingen konkrete føringer til
	utforming og valg av teknologier. 
\end{itemize}

På bakgrunn av de to sistnevnte faktorene, bestemte vi oss tidlig for å
fokusere på smidige over plandrevne modeller. Plandrevne modeller
gjennomfører utviklingsfasene i \gls{SDLC} individuelt og i kronologisk
rekkefølge. Modellene legger opp til at endringer ikke blir gjort i tidligere
fullførte faser dersom problemer oppdages. I smidige modeller er det heller lagt
opp til å gjennomføre fasene parallelt, som gjør det enklere å ta hensyn til
nye krav eller revidere tidligere beslutninger med ny informasjon. Denne
arbeidsflyten kombinert med vår begrensede erfaring og muligheten til å lære av
tidligere utviklingsperioder, gjør at vi anser smidige modeller er mest aktuelle for vårt
prosjekt.

Mer spesifikt valgte vi i all hovedsak å se på Scrum og Kanban, da vi hadde
kjennskap til begge disse modellene fra tidligere i studieløpet. Gjennom Scrum sine
\glspl{sprint} kan vi planlegge hva utviklingsfokuset skal være for hver
utviklingsperiode. Etter hver sprint legges det opp til å vurdere hvordan
perioden gikk gjennom en \gls{sprint retrospective}. Formålet med dette
møtet er å lære av gruppens feil og erfaringer, slik at videre arbeid blir
bedre. Samtidig var det også aspekter med Scrum vi anså som mindre optimale.
Fundamentet i Scrum bygger på å definere etablerte roller. Med en
gruppestørrelse på tre medlemmer anså vi risikoen for at mye tid ville gå til
prosesstyring som høy. Spesielt siden ingen på gruppen hadde erfaring med rollene
fra tidligere, kunne dette ta verdifull tid bort fra utvikling.

Dette gjorde at vi undersøkte muligheten for å kombinere \gls{lean}
metodikker med Scrum. Her peker Kanban seg ut som et populært alternativ. Kanban
ofrer noe av strukturen som Scrum tilbyr, i et forsøk på å maksimere
effektivitet og flyt i utviklingen. Hovedfokus i Kanban ligger på å optimalisere
arbeidsflyt gjennom visualisering i det som kalles en Kanban-tavle~\cite{scrumandkanban}.
Tavlen fungerer ved å flytte oppgaver mellom predefinerte kolonner, med
begrensinger for hvor mange oppgaver som kan være i hver
kolonne~\cite{scrumandkanban}. Formålet med dette er å sikre at oppgaver ikke
setter seg fast eller glemmes. Ulempene med Kanban er at det kan være for lite struktur
for oppgaver som har en klart definert tidsramme og at tavlen fort blir
utdatert på grunn av manglende oppfølging.

Scrum gir oss et godt grunnlag for å helhetlig styre prosjektet, mens Kanban
er god på daglig drift. En kombinasjon av disse modellene, ofte kalt Scrumban,
gir en utviklingsmodell som tilfredsstiller våre krav og rammer. Vi bestemte dermed
oss for å gå for Scrumban som utviklingsmodell.

\subsection{Gjennomføring}
\label{sec:gjennomforing}
Vi valgte å gjennomføre sprinter på to uker, hvor vårt bi-ukentlige \gls{sprint
review meeting} med oppdragsgiver markerte slutten av hver sprint. Formålet med
disse møtene var å presentere resultatet av den foregående sprinten og få
tilbakemelding på det vi hadde gjort. På slutten av hver sprint gjennomførte vi
et \gls{sprint retrospective} møte for å reflektere over den forbigåtte
sprinten. 


De retrospektive møtene fungerte som en naturlig overgang til våre \gls{sprint
planning} møter. Disse gjennomførte vi før hver sprint, der målet var å definere
hvor fokusområdet lå for den kommende sprinten. Her definerte vi konkrete mål
for perioden og hentet nye oppgaver fra \glspl{product backlog}. Gjennom hver sprint
hadde vi også ukentlige møter med veileder, hvor vi også mottok tilbakemelding
på arbeidet vårt, men hvor fokus var mer rettet mot leveransen til NTNU\@. På
alle eksterne møter ble det ført referat etter vår definerte stilguide
(se figur \ref{fig:moteref}). Dermed var det enkelt å tilegne seg informasjon fra
tidligere referater, dersom dette var nødvendig. Eksempler på møtereferater
kan sees i vedlegg~\ref{appendix:oppstartsmøte_veileder} og~\ref{appendix:møtereferat_argos}.


\begin{figure}[h!] 
    \centering
    \includegraphics[width=0.8\textwidth]{moteref.PNG} 
	\caption{Stilguide for møtereferatene.} 
    \label{fig:moteref}
\end{figure}

\subsubsection{Arbeidsflyt}
Da prosjektet i sin helhet består av separerte deler, er det hensiktsmessig å
organisere relaterte arbeidsoppgaver i samlede bolker. For å gjøre dette
valgte vi å bruke Jira sine \glspl{epic}. Vi lagde en epic for hver av
hovedkategoriene i oppgaven og delte disse inn i konkrete
\glspl{issue}. Dette medførte at vi lettere kunne holde oversikt
over progresjon i prosjektet. Ved hvert \gls{sprint planning} lagde vi
oppgaver som videre ble koblet til tilhørende epics før de ble plassert i
\gls{product backlog}. Oppgavene ble etter beste evne delt opp i like store deler, og vi
hadde som mål at gjennomføring skulle ta mellom tre og fem dagsverk per
oppgave. For å opprettholde oversikten over oppgavene brukte vi Jira og dens
tilhørende Kanban-tavle (se figur~\ref{fig:kanban}). Tavlen valgte vi å dele inn
i fem kolonner:


\begin{itemize}
    \item \textbf{Selected for development} \\
    Oppgaver som er hentet fra \gls{product backlog}, men som enda ikke er påbegynt.
    \item \textbf{In progress} \\
	Påbegynte oppgaver.
    \item \textbf{Waiting} \\
	Oppgaver som er påbegynt, men som nå venter på en ekstern faktor før
	arbeid kan fortsette. Et eksempel på dette kan være at oppklaring til
	funksjonalitet kreves fra oppdragsgiver.
    \item \textbf{Review} \\
	Oppgaver som er ferdige, men som venter på gjennomgang av de andre
	gruppemedlemmene for kvalitetssikring. Før en oppgave skulle plasseres i
	denne kategorien, satt vi krav om at det skulle foreligge en \gls{PR}.%(REF HER TIL
	%AVDELING HVOR VI I FREMTIDEN SNAKKER OM GIT OG DEN PROSESSEN).
    \item \textbf{Done} \\
	Ferdigstilt oppgaver. Vi anså en oppgave som ferdigstilt dersom den hadde
	blitt klarert av begge de andre medlemmene på gruppen. Dersom
	det oppsto behov for endringer, åpnet vi nye issues som igjen gikk gjennom
	den samme prosessen beskrevet over.  
\end{itemize}

\begin{figure}[h!] 
    \centering
    \includegraphics[width=\textwidth]{kanban.png} 
	\caption{Kanban-tavlen med inndeling etter medlem.} 
    \label{fig:kanban}
\end{figure}

For hver av kolonnene begrenset vi antallet oppgaver som den
kolonnen kunne inneha til enhver tid. Formålet med dette
var å forhindre en oppbygning av halvferdige oppgaver og eventuelle
flaskehalser. Denne grensen satt vi initielt til å være seks, slik at hvert
medlem på gruppen kunne ha to aktive oppgaver i hver kategori. 

\begin{figure} 
    \centering
    \includegraphics[width=0.7\textwidth]{flow.png}
    \caption{Visualisering av arbeidsflyt.} 
    \label{fig:arbeidsflyt}
\end{figure}

Tallet seks ble i all hovedsak satt som et grovt utgangspunkt, der grensene
kunne justeres gjennom prosessen. For å skape rom for et flertall
mindre oppgaver, endte vi med å øke grensen for Selected for Development til ni
i stedet for seks. På samme vis reduserte vi det maksimale antallet i Review, da
vi anså det hensiktsmessig for arbeidsflyten å raskere gjennomgå ferdigstilte
oppgaver. 

\subsubsection{Fremdrift}
\label{sec:fremdrift}
Vi delte utviklingsprosessen inn i seks milepæler:

\begin{enumerate}
	\item \textbf{31. januar:} Ferdigstilt forprosjekt
	\item \textbf{15. februar:} Ferdigstilt vurdering av skyløsning
	\item \textbf{1. mars:} Første prototype
	\item \textbf{22. april:} Konkludering av utvikling \\
	- Ingen ny funksjonalitet blir påbegynt, fokus på finpuss og testing
	\item \textbf{6. mai:} Ferdigstilt produkt  \\
	- Testing og koding ferdigstilles, fokuset går over på rapportskriving
	\item \textbf{20. mai:} Ferdigstilt rapport
\end{enumerate}

På hvert \gls{sprint planning} gjennom prosjektløpet, brukte vi milepælene som
målestokk for vår overordnede progresjon. I hver sprint satt vi konkrete mål
som jobbet mot de konkrete milepælene. Under er en overordnet oversikt over hva
vi jobbet med i hver sprint. I denne oversikten har vi valgt å inkludere en
sprint 0, selv om vi på dette tidspunktet i prosessen enda ikke hadde
begynt å jobbe i sprinter. Arbeid gjort før starten av denne var i all hovedsak
administrative oppgaver relatert til oppstart, og er ikke inkludert i
oversikten.

\begin{itemize}
    \item \textbf{Sprint 0:} 23/01 - 06/02
    \begin{itemize}
        \item Planlegging
        \item Forprosjekt
        \item Start skyundersøkelse
    \end{itemize}

    \item \textbf{Sprint 1:} 06/02 - 20/02
    \begin{itemize}
        \item Ferdigstilt skyundersøkelse
        \item Start rammeverk
        \item Start Grafana
        \item Undersøkte teknologier for rapportgenerering
    \end{itemize}

    \item \textbf{Sprint 2:} 20/02 - 06/03
    \begin{itemize}
        \item Rammeverk for aksessering av data
        \item Grafana direkte mot databasen
        \item Start simulering med resultater i Grafana
        \item Grafana Reporter 
    \end{itemize}

    \item \textbf{Sprint 3:} 06/03 - 20/03
    \begin{itemize}
        \item Start dataflyt gjennom API til Grafana
        \item Intervju med salgsavdeling
        \item Undersøk videreutvikling av nåværende rapportløsning
        \item Start \gls{proof_of_concept} rapport
        \item Opprette bachelorrapport
        \item Utvidelser simulering og rammeverk
    \end{itemize}

    \item \textbf{Sprint 4:} 20/03 - 10/04
    \begin{itemize}
        \item Ferdigstilt rapport proof of concept med dataflyt gjennom API
        \item Ferdigstilt dataflyt gjennom API til Grafana
        \item Start heatmap
        \item Introduksjonsdelen av bachelorrapport
        \item Start klassedelt simulering
        \item Utvidelser rammeverk
    \end{itemize}

    \item \textbf{Sprint 5:} 10/04 - 24/04
    \begin{itemize}
        \item Start hoveddel bachelorrapport
        \item \Gls{refactor} dashbord-kode 
        \item Ferdigstilt klassedelt simulering
        \item Forbedring av rammeverk
    \end{itemize}

    \item \textbf{Sprint 6:} 24/04 - 08/05
    \begin{itemize}
        \item refactor rapport-kode 
        \item Ferdigstilt hoveddel bachelorrapport
        \item refactor tester 
        \item Start rapport drøfting
    \end{itemize}

    \item \textbf{Sprint 7:} 08/05 - 20/05
    \begin{itemize}
        \item Ferdigstilt bachelorrapport
    \end{itemize}
\end{itemize}

\subsubsection{Tidsbruk}
For å reflektere over hvordan tiden ble brukt gjennom prosjektløpet, har vi
loggført alle arbeidstimer i Excel. Tiden ble ført per påbegynte halvtime og
lunsjpauser ble ikke inkludert. I loggføringen valgte vi å skille mellom arbeid
vi gjorde felles, og det som ble gjort på egenhånd. I tillegg brukte vi følgende
fem kategorier for å ytterligere gruppere: 

\begin{itemize}
    \item \textbf{Administrativt} \\
    Administrativt arbeid som føring av referater, interne møter eller
    planlegging. 
    \item \textbf{Litteratur} \\
    Tilegning av kunnskap gjennom litteratursøk. Eksempelvis lesing av
    akademiske tekster eller tidligere bacheloroppgaver. 
    \item \textbf{Programmering} \\
    Utvikling av sluttproduktet og andre koderelaterte aktiviteter. 
    \item \textbf{Rapport} \\
    Skriving og korrekturlesing av rapport.
    \item \textbf{Møte} \\
    Eksterne møter med veileder og oppdragsgiver.
\end{itemize}

\subsection{Verktøy}
Gjennom prosjektløpet har vi brukt en rekke verktøy. De mest sentrale verktøyene
er inkludert i figur \ref{fig:verktoy}.

\begin{figure}[h!] 
    \centering
    \includegraphics[width=\textwidth]{verktoy.PNG} 
	\caption{De mest sentrale verktøyene i prosjektet.} 
    \label{fig:verktoy}
\end{figure}

\subsubsection*{Utvikling og versjonskontroll}
\begin{itemize}
    \item \textbf{PyCharm} Utvikling i Python.
    \item \textbf{Visual Studio Code} Rapportskriving.
    og samarbeid. 
    \item \textbf{Git} Versjonskontroll.
    \item \textbf{BitBucket} Lagring av kildekode.
    \item \textbf{GitLab} Pipeline.
    \item \textbf{Docker} Testing og pipeline.
    \item \textbf{OpenStack} Intern sky for testmiljø. 
\end{itemize}

\subsubsection*{Administrative hjelpeverktøy}
\begin{itemize}
    \item \textbf{Jira} Styring og visualisering av arbeidsoppgaver.
    \item \textbf{Excel} Loggføring av arbeidstimer.
    \item \textbf{Confluence} Dokumentasjon av arbeid, blant annet
    møtereferater, løpende dokumentasjon og diagrammer. 
    \item \textbf{Teams} Muntlig kommunikasjon mellom gruppen, veileder og oppdragsgiver.
\end{itemize}

\subsubsection*{Tekniske hjelpeverktøy}
\begin{itemize}
    \item \textbf{Sphinx} Automatisk generering av dokumentasjon av kildekode i
    HTML-format.
    \item \textbf{Swagger} Dokumentasjon og debugging av endepunktene. 
\end{itemize}

\subsection{Risikoanalyse} 
Under følger vår identifikasjon og analyse av situasjoner som kan oppstå i
løpet av prosjektperioden. Disse situasjonene er beskrevet og deretter knyttet
opp mot tilhørende risiko og konsekvens. Videre er tiltak formulert.
Sannsynlighet og konsekvens er klassifisert etter skala lav, middels og høy. 

\begin{enumerate}
    \item \textbf{Tidsfrist overskrides} \\ 
    Uforutsette hendelser som tap av arbeid, alvorlig sykdom, tekniske
    problemer eller mangel på kompetanse kan føre til forsinkelser i
    prosjektets fremdrift. Sannsynlighet vurderes som middels og konsekvens
    vurderes som høy.
    
    \item \textbf{Overestimert omfang} \\
    Gruppas manglende erfaring med prosjekter av denne størrelsen, gjør at
    tidsestimering er vanskelig. Sannsynlighet vurderes som lav og
    konsekvens vurderes som middels.
    
    \item \textbf{Tap av arbeid ved systemkræsj eller uaktsomhet} \\
    Tap av arbeid i utviklingsprosessen kan i verste fall ha katastrofale
    ringvirkninger. Sannsynlighet vurderes som lav og konsekvens vurderes som
    høy.

    \item \textbf{Mangel på kompetanse} \\
    Gruppa har lite erfaring med prosjekter av denne størrelsen, og vi anser
    det som høyst sannsynlig at vi vil støte på arbeidsoppgaver som krever at
    vi må tilegne oss ny kunnskap. Sannsynlighet vurderes som høy og
    konsekvens vurderes som middels.
    
    \item \textbf{Tap av gruppemedlemmer} \\
    Alvorlig sykdom, ulykker eller at gruppemedlemmer 
    slutter kan medføre at gruppenss kapasitet blir kraftig 
    redusert. Sannsynlighet vurderes som lav og konsekvens vurderes som høy.
    
    \item \textbf{Konflikter innad i gruppen} \\
    Uenigheter eller andre samarbeidslige faktorer underveis i prosjektløpet
    kan skape konflikter innad i gruppen. Sannsynlighet og konsekvens vurderes
    som lav.
    
    \item \textbf{Data fra oppdragsgiver kommer på avveie} \\
    Dersom denne dataen kommer på avveie vil det kunne direkte påvirke kunden
    samt reflektere negativt på Argos. Sannsynlighet vurderes som lav og
    konsekvens vurderes som høy.
    
    \item \textbf{Oppdragsgiver endrer krav underveis} \\
    Under utviklingsprosessen er det sannsynlig at enkelte krav endres,
    fjernes eller at det oppstår nye krav. Sannsynlighet vurderes som høy og
    konsekvens vurderes som lav.

    \item \textbf{Mangel på datagrunnlag i simulering} \\
    Oppdragsgiver har ingen eksisterende løsning innen simulering, og det
    foreligger derfor mulighet for at datagrunnlaget for å gjøre etterspurte
    simuleringer ikke er tilstede. Sannsynlighet vurderes som lav og konsekvens
    vurderes som middels. 
\end{enumerate}

\begin{table}[ht]
\centering
\begin{tabular}{cl|l|l|l|}
\cline{3-5}
\multicolumn{1}{l}{}&&\multicolumn{3}{c|}{Sannsynlighet}\\\cline{3-5} 
\multicolumn{1}{l}{}&&Lav&Middels&Høy\\\hline

%Første rad
\multicolumn{1}{|c|}{}& Lav & 6 \cellcolor[HTML]{01a966} & 2, 9 \cellcolor[HTML]{01a966} & 3, 5, 7 \cellcolor[HTML]{01a966} \\ \cline{2-5} 

%Andre rad
\multicolumn{1}{|c|}{}& Middels & \cellcolor[HTML]{01a966} & \cellcolor[HTML]{e4db19} & 1 \cellcolor[HTML]{e4db19} \\ \cline{2-5}

%Tredje rad
\multicolumn{1}{|c|}{\multirow{-3}{*}{Konsekvens}}& Høy & 8 \cellcolor[HTML]{01a966} & 4 \cellcolor[HTML]{e4db19} & \cellcolor[HTML]{a90046} \\ \hline

\end{tabular}
\caption{Risikovurdering for prosjektarbeidet.}
\label{fig:risikovurd}
\end{table}

Fra risikovurderingen (se tabell~\ref{fig:risikovurd}) peker risiko 1 og 4 seg ut
som høy risiko, og krever derfor mitigerende tiltak. Disse tiltakene beskrives i
tabell~\ref{fig:tiltak}.

\begin{table}[ht]
\centering
\begin{tabular}{ | l | l | m{8cm} | } 
\hline
\#&Risiko&Tiltak \\
\hline
1 & Tidsfrist overskrides & Stort fokus på planlegging, både initielt, men også
gjennom prosjektløpet ved retrospektive møter. Her vil satte milepæler bli brukt
for å måle progresjon. Hyppige møter (\ref{sec:fremdrift}) med veileder og
oppdragsgiver vil også gjennomføres for ekstern tilbakemelding. \\
\hline
4 & Mangel på kompetanse & Gruppa vil samarbeide tett gjennom hele
prosjektløpet. Der tilsvarende verktøy vurderes, vil vi også betrakte egen
kompetanse og verktøyenes læringskurver.\\
\hline
\end{tabular}
\caption{Tiltak med tilhørende beskrivelse.}
\label{fig:tiltak}
\end{table}
