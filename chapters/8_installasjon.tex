I dette kapittelet vil vi gå nærmere inn på hvordan systemet kan bygges, og hvordan man
kan benytte støtteverktøyene Swagger og Sphinx. Til slutt beskrives hvordan skymiljøer
skiller seg fra lokal utrulling med tanke på ytelse, risiko og kostnad.

\subsection{Bygging}

Som nevnt i implementasjon
er rammeverket utformet som en Python pakke. Denne pakken administreres gjennom Poetry.
Poetry er et verktøy som hjelper utviklere håndtere pakkers avhengigheter og konfigurasjon. 

Når en pakke er klar for utrulling, vil Poetry konvertere pakken med 
\lstinline{poetry build}. Resultatet av dette kan deretter installeres direkte på andre
maskiner med \lstinline{pip install (...)} eller lastes opp til en \gls{package manager}. 
Når pakken er installert kan API-et startes med 
\lstinline{python3 -m ArgosAnalysePlatform (...)}

Denne byggeprosessen kan gjøres i GitLab pipelines på samme måte som linting og
testing fra kapittel~\ref{chapter:testing}. Byggingen har blitt lagt som et ekstra steg til
slutt. Dette medfører at bygging ikke gjøres med mindre prosjektet ellers har god
kvalitet. Listing~\ref{listing:additional_pipeline_build} beskriver hvordan
\lstinline{.gitlab_ci.yaml} må utvides for å oppnå dette.\\

\begin{lstlisting}[
    caption={Bygging av Python pakke i GitLab pipeline.},
    label={listing:additional_pipeline_build}
]
stages:
  - linting_and_formatting
  - testing
  - build
  - ci_status

# ...

build:
  stage: build
  only:
    - master
  before_script:
    - *python_before_script
  script:
    - poetry build
  artifacts:
    paths:
      - dist
    expire_in: 2 days
\end{lstlisting}


\subsection{Dokumentasjon}

Rammeverket i analyseplattformen er dokumentert ved hjelp av verktøyet Sphinx. Dette
er et verktøy med tilhørende formateringsstandard som kan generere brukervennlig
dokumentasjon basert på kildekoden direkte~\cite{sphinx}. Fordelene med at dokumentasjonen ligger
direkte i kildekoden er at den er enklere å holde vedlike, som gjør at den er enklere å
benytte av nye og erfarne utviklere~\cite{modern_technical_writing}. 

For å tolke dokumentasjonen korrekt må den formateres etter Sphinx sin standard.
I listing~\ref{listing:docstring_example} vises noen utvalgte nøkkelord.

\begin{lstlisting}[language=Python, caption={Docstring-eksempel.}, label={listing:docstring_example}]
def generate_pareto_plot(x: List[int], y: List[int]) -> plt.Figure:
    """Generate a pareto plot from the supplied data

    :param x: The x axis
    :param y: The y axis

    :return: The figure of the plotted data.
    """
    ...
\end{lstlisting}

Sphinx vil tolke denne docstringen og konvertere den til et brukervennlig
format som kan sees i figur~\ref{fig:docstring_html}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{docstring_html.png}
    \caption{docstring eksempel i HTML-format~\label{fig:docstring_html}.}
\end{figure}

Analyseplattformens pipeline er satt opp til å generere Sphinx-dokumentasjonen
i samme stadie som byggingen av Python pakken. Dette gjøres ved å legge inn
den ekstra konfigurasjonen i listing~\ref{listing:docs_pipeline}.

\begin{lstlisting}[caption={Bygging av Sphinx dokumentasjon.}, label={listing:docs_pipeline}]
docs:
  stage: build
  before_script:
    - *python_before_script
  script:
    - sphinx-apidoc -o sphinx/source/ ArgosAnalysePlatform/ -eFa -A Argos
    - cp sphinx/conf.py sphinx/source/conf.py
    - make html -C sphinx/source

  artifacts:
    paths:
      - sphinx/source/_build/html
\end{lstlisting}

For å dokumentere endepunktene til API-et har FastAPI inkludert Swagger~\cite{swagger}.
Swagger er et verktøy som genererer dokumentasjon for hvert API-endepunkt i et
web-grensesnitt, og gir brukeren mulighet til å gjøre spørringer mot
endepunktene rett i nettleseren. Dette gjør det enkelt for utviklere å se
utfallet av spørringer med forskjellige parametre, uten å måtte bruke andre
verktøy. Figur~\ref{fig:swagger_example} viser et eksempel på dette for
\lstinline{/query} endepunktet.

I tillegg til å lese docstrings av dataklassene og endepunktene som FastAPI
tjener, vil også Swagger vise ekstra dokumentasjon som er spesifisert i FastAPI
parametre, som for eksempel
\lstinline{start_time: str = Form(..., description="Start time in isoformat")}. 
For å unngå å repetere den samme informasjonen flere ganger har vi antatt at
brukeren leser dokumentasjonen i Sphinx, og referert til at det finnes mer i
Swagger.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{swagger_query_endpoint.png}
    \caption{Swagger representasjon av /query \label{fig:swagger_example}.}
\end{figure}

\subsection{Produksjonssetting i sky}

Argos ønsket at Koios skulle kunne implementeres både på en lokal tjener, men
også i skyen. Dette medførte at Argos ønsket at vi gjorde en grunnleggende
undersøkelse der vi sammenlignet lokale og skyløsninger med tanke på ytelse,
risiko og kostnad. Hele undersøkelsen kan sees i
vedlegg~\ref{appendix:skyundersokelse}.

I undersøkelsen satt vi `Hvordan påvirker faktorene ytelse, risiko og kostnad
en migrering fra lokal infrastruktur til sky?' som innledende problemstilling.
For denne undersøkelsen var det ikke ønskelig fra Argos at vi gikk i dybden,
men heller fokuserte på de overordnede vurderingene. Vi satt derfor en grense
på 2 uker til å utføre undersøkelsen, da arbeidet raskt kan øke i størrelse
og omfang.

Ved å undersøke akademiske rapporter og statlige utredninger, fant vi at
skytjenester har en fordel over lokal infrastruktur med sin dynamiske ytelse,
men har svakheter vedrørende krav om pålitelig internettforbindelse.
Skyplattformer stiller også sterkere med tanke på risiko, da leverandørende
ofte tilbyr sin sikkerhetsekspertise som en tjeneste. Det er ikke tydelige tegn
på at infrastruktur som er rullet ut i skyen er mer utsatt for
sikkerhetshendelser enn tilsvarende utrulling lokalt. Kostnadsmessig er det
også en fordel å kjøre i skyen, da forretningsmodellen legger opp til at man
bare betaler for nøyaktig de ressursene som benyttes, i motsetning til å måtte
investere i maskinvare.

