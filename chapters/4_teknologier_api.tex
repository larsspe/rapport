Oppgaven fra oppdragsgiver la ingen føringer for valg av teknologi eller
arkitektur. Dette medførte at mye tid ble brukt på å finne de teknologiene som
passet med kravene til systemet, avveiet av hvor mye vi kjente til de fra før
og hvor enkle de var å anvende. Da dette prosjektet var stort og ukjent for
oss, var det viktig for oss å velge teknologier som la til rette for for
rask prototyping.


\subsection{Programmeringsspråk}

Et av de første valgene vi tok var hvilket programmeringsspråk vi skulle bruke.
Valget la grunnlaget for andre viktige teknologivalg senere.  Vi stilte
følgende kriterier til språket:

\begin{itemize}
\setlength\itemsep{0em}

\item{\textbf{Støtte for tredjepartsbiblioteker} For å tilrettelegge for rask
prototyping var det viktig at språket hadde mange tredjepartsbiblioteker 
vi kunne benytte oss av.}

\item{\textbf{Kompabilitet på tvers av platformer} Argos ønsker at systemet
skal kunne flyttes over på andre plattformer uten større arbeid, for eksempel
til sky.}

\item{\textbf{Tidligere erfaringer} Dersom valget falt på et språk som gruppen
ikke var kjent med, ville det medført ekstra tid til å lære språket.
Det var også en fordel om Argos allerede hadde kjennskap til språket.}

\item{\textbf{Sammenkobling med Argos' eksisterende teknologier} Det hadde vært
en fordel om språket integrerte med Argos' eksisterende systemer. }

\end{itemize}

Alternativene vi undersøkte var C++, Python og Java. Vi tok utgangspunkt
i disse alternativene da gruppen har mer eller mindre kjennskap til disse
fra før. Selv om det finnes andre programmeringsspråk som kunne
vært relevante, for eksempel Go eller Rust, fant vi det ikke hensiktsmessig
å lære oss disse med mindre det ga store fordeler for prosjektet vårt.

Fordelene med C++ var at Argos allerede hadde mye kjennskap til dette språket i
bedriften. Dette gjør at systemet vårt enklere kunne integreres i Argos'
kjerneplattform dersom dette skulle være ønskelig. Språket eksekverer raskt,
som kan være en fordel dersom prosjektet vil innebære tunge algoritmer.
Ulempene med språket var at gruppen har minst kjennskap på området av de tre
overnevnte. C++ biblioteker kan variere mellom operativsystemer, noe som kan gjøre
det vanskeligere å flytte systemet til en annen plattform ved behov. På grunn
av denne kompleksiteten, er språket også lite egnet for rask prototyping.

Java er et språk som konsekvent scorer høyt på de fleste programmeringsindekser
\cite{tiobe_language_statistics, redmonk_languages_statistics,
ieee_languages_statistics, codingdojo_languages_statistics}. Språket er
modulært og gir gode muligheter for ekspansjon med sin objektorienterte
struktur. Det finnes mange utvidelser og rammeverk som gjør språket meget
allsidig. Java er plattformuavhengig med sin \gls{JVM}, som gjør systemet
uavhengig av operativsystemet det kjøres på.  Ulempene vi fant med Java var at
gruppen hadde noe kunnskap om det, men at mer måtte læres for å bruke det
effektivt. Språket brukes heller ikke av Argos idag, noe som vil medføre ekstra
ressurser fra de ansattes side for å lære seg språket etter at
bachelorprosjektet er over.


Det siste alternativet vi undersøkte var Python. Python er et språk som er
velkjent for å være enkelt å lære seg og å forstå for personer uten tidligere
erfaring~\cite{pythonlearn}. Språket har bred støtte gjennom utvidelser og
rammeverk som gir en bruker rask mulighet til å benytte språket for ellers
komplisert funksjonalitet. Populære eksempler på dette er biblioteker som
OpenCV for bildebehandling og PyTorch for maskinlæring. Språket er
objektorientert i samme stil som Java, med mange av de samme fordelene. Det
finnes Python tolkere for alle de store operativsystemene, som i praksis gjør
den plattformuavhengig. I motsetning til C++ og Java som kompileres, fungerer
Python heller ved at koden tolkes linje for linje når den eksekveres. Dette
medfører betydelig redusert ytelse ved prosessorintensive
algoritmer~\cite{python_performance}.  Python har ikke statiske variabeltyper
som Java og C++, noe som åpner for flere problemer ved eksekvering som ellers
kan detekteres av en kompilator på forhånd.


% Binde de siste to setningene opp mot vårt prosjekt

Basert på disse fordelene og ulempene valgte vi å gå for Python. Python og Java
dekket alle de tekniske kriteriene vi satt for programmeringsspråk. På det rent
tekniske så vi at Java muligens hadde vært det beste valget, men helhetlig falt
vi til slutt på Python, da våre erfaringer og Argos' behov for vedlikehold i
etterkant overveide fordelene til Java. Muligheten for rask prototyping var
også en betydelig faktor.

\subsection{API-arkitektur}
\label{section:teknologi_api_rammeverk}
Systemet vårt legger opp til å kjøre lokalt og i skyen. Vi så også
umiddelbart at vi ville komme til å sende og motta data fra flere forskjellige
eksterne systemer. Dette gjorde at en \gls{HTTP}-basert arkitektur virket mest
aktuell. 

Det finnes to hovedalternativer for API-baserte løsninger for web: \gls{REST} og
\gls{GraphQL}. \gls{REST} er eldst, og dermed mest
veletablert. REST kan regnes som en utvidelse av HTTP ved at den kan ta argumenter
enten i URL-en direkte, gjennom http-form eller som et \gls{JSON}-objekt. Det
helhetlige API-et deles opp i mindre endepunkt som returnerer ønsket data. REST
kan av denne grunn brukes av alle tjenester som støtter HTTP-oppslag. GraphQL
er en nyere standard som forsøker å ha bedre ytelse enn REST~\cite{graphql}, ved at man gjør
spørringer (derav navnet Query Language) mot ett endepunkt som henter ut
nøyaktig den dataen man ønsker. Ettersom GraphQL er spørringsbasert vil det
kreve at klienter har konkret støtte for det.

Vi valgte å fokusere på REST da dette er en enklere protokoll å benytte. Det
ga større sannsynlighet for at flere eksterne systemer kunne bruke API-et, da
langt flere systemer støtter HTTP-oppslag enn GraphQL-spørringer. 


\subsection{Python rammeverk}

Som nevnt tidligere er bredden av tilgjengelige tredjepartsbiblioteker en av de
største styrkene for Python. Her vil vi gå gjennom noen av de mest sentrale
bibliotekene vi har utforsket og benyttet.

\subsubsection{SQL rammeverk}

En ramme vi måtte forholde oss til var at eksisterende data ligger i en instans
av SQL Server 2012. Det finnes to Python rammeverk for dette: pyodbc
og pymssql~\cite{microsoft_pyodbc}.  Microsoft prioriterer å støtte pyodbc, så
vi valgte å gå for dette alternativet.

På toppen av pyodbc var gruppen allerede kjent med en SQL-verktøykasse som
heter SQLAlchemy~\cite{sqlalchemy}. SQLAlchemy tilbyr et standardisert
grensesnitt for å generere spørringer, uavhengig av underliggende database.
Denne modellen utheves spesielt i \gls{ORM}
modulen. ORM er en implementasjon av data mapper designmodellen. Denne
modellen oppretter et direkte forhold mellom en datakilde og objektorienterte
klasser. Når disse klassene brukes i koden, fjernes avhengigheter mellom bruk av
dataen og kilden. Denne abstraheringen gir oss en stor fordel ved at det for
eksempel er enklere å bytte database i bakgrunnen, uten at større endringer
kreves i koden. I figur~\ref{fig:orm_diagram} kan man se et eksempel på
dette.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{orm_diagram.png}
    \caption{\label{fig:orm_diagram} Eksempel på ORM struktur. }
\end{figure} 


\subsubsection{API-rammeverk}

Det finnes i hovedsak tre alternativer for å implementere API-rammeverk
i Python: Django, Flask og FastAPI\@. Vi vurderte disse rammeverkene utifra følgende
hovedkriterier:
\\

\textbf{Funksjonalitet}, eller hvor mye funksjonalitet er bygd inn/tilgjengelig
fra tredjepartsbiblioteker. Her er Django sterkest, med sitt fokus på å tilby
utviklere en rask måte å sette opp en fullstendig web-stack~\cite{django}.
FastAPI kommer som nummer to med sine brede innebygde verktøy spesifikt for API
utvikling. Eksempler her er Swagger, Redoc, OAuth 1/2 og
OpenAPI~\cite{fastapi_features}. Flask kommer sist da hovedfokuset til flask er
å tilby det absolutt minste som trengs for å kjøre en web-tjener~\cite{flask},
med tredjepartsbiblioteker for ytterligere funksjonalitet. 
\\

\textbf{Ytelse}, eller hvor raskt gjennomføres prosesseringen av spørringene.
FastAPI kommer tydelig ut som best i denne kategorien. FastAPI bygger på
Starlette, som igjen bygger på Uvicorn. Uvicorn er regnet av blant annet
Techempower for å være en av de raskeste webtjenerene for Python. I samme test
fikk FastAPI 1209 poeng, Flask fikk 468 og Django fikk
280~\cite{framework_performance_overview}. \\

\textbf{Kompleksitet}, eller hvor enkelt eller vanskelig det er å benytte seg av
funksjonaliteten, både på enkelt og avansert nivå. Her kommer FastAPI best ut
hovedsakelig på grunn av sin kjernefilosofi om at bruk skal være enkel og
intuitiv. Flask, derimot, er bygget på at utvikleren må implementere
ekstrafunksjonalitet selv.
\\

Utifra disse kriteriene bestemte vi oss for at FastAPI passer best for
analyseplattformens formål. Vi trenger bare funksjonalitet for API-endepunkter,
i tillegg til lett tjening av statiske filer. Django vil derfor tilby mye
unødvendig funksjonalitet. FastAPI kommer også med funksjonalitet for
dokumentasjon og typesjekking, som vi anså som nyttig. FastAPI regnes også som
raskere enn alternativene, noe som er en fordel i skybaserte miljøer der ytelse
direkte påvirker kostnad. 
