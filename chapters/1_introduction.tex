\subsection{Bakgrunn}
\begin{displayquote}
``Argos Solutions AS er et globalt høyteknologi-selskap som holder til på
Kongsberg. Selskapet utvikler og produserer avanserte systemer for automatisk
visuell inspeksjon til ledende treplateprodusenter verden over. Systemene
benyttes i fabrikker for å automatisere og optimalisere
produksjonsprosesser.''~\cite{lovenskiold}
\end{displayquote}

Levering og drifting av det automatiserte inspeksjonsverktøyet Argos
Grading System er del av Argos' kjernevirksomhet. Systemet analyserer
forskjellige typer treplater og gir en vurdering av kvalitet basert på
fabrikkens krav. Dataene om hver enkelt plate og informasjon om eventuelle 
defekter lagres i en database.

For å vise resultatet av denne prosessen for ikke-teknisk personell, finnes
det i dag en løsning som genererer rapporter basert på forhåndsdefinerte
maler. Over tid har det blitt oppdaget flere svakheter ved denne løsningen.
De forhåndsdefinerte malene inneholder data som ofte ikke er interessant for
kunden, og det er vanskelig å endre disse etter fabrikkenes individuelle
behov. Det er dermed vanskelig for kundene å benytte seg av innsamlede data
for å få innsikt i produksjonsprosessen.

Argos er derfor interessert i å utvikle en analyseplattform for å håndtere dette.
Gjennom plattformen ønsker Argos å tilby sanntidsstatistikk, et mer
fleksibelt rapporteringssystem og simuleringsmuligheter. Ambisjonen er at
disse verktøyene skal gi kunden enklere tilgang til produksjonsdata og dermed
øke deres utbytte av Argos' tjenester.

\subsubsection{Hvordan brukes Argos Grading System?} Argos Grading
System maskinene, som vi herfra og ut vil referere til som \glspl{skanner}, står
helt i senter av vår oppgave. Disse maskinene samler inn all dataen vi
jobber mot, og vi anser det derfor som viktig å gi en kort innføring i
hvordan de fungerer og hvordan de brukes i praksis.

\begin{figure}[h!] 
    \centering
    \includegraphics[width=\textwidth]{argos_scanner.jpg} 
	\caption{Argos Grading System skanner~\cite{argoshome}.} 
    \label{fig:argos_scanner}
\end{figure}

Ved hjelp av høyoppløselige kameraer og lyskilder i forskjellige vinkler,
opparbeider skannerne (se figur~\ref{fig:argos_scanner}) seg et helhetlig bilde
av hver plate~\cite{argoshome}.  Denne informasjonen settes opp mot produktets
krav, og brukes videre for å kategorisere platene etter
kvalitet\footnote{Kvalitet beskriver her graden av samsvar med kravene satt
av kunde.}.  Kategoriseringen brukes deretter av eksterne systemer for å
sortere ut plater (se figur~\ref{fig:argos_sort}) som må repareres eller vrakes.

\begin{figure}[h!] 
    \centering
    \includegraphics[width=\textwidth]{argos_sort.jpg} 
	\caption{Skanner med tilhørende sortering ved hjelp av
	sugekopper~\cite{argoshome}.}
    \label{fig:argos_sort}
\end{figure}

Det er ofte flere skannere per fabrikk, og de er gjerne plassert på
kritiske punkter i produksjonslinjen - eksempelvis etter påføring av laminat
eller pussemaskin. På bakgrunn av dette er det, gjennom data som samles inn av
skannerne, mulig å danne et helhetlig bilde av produksjonen. Det er derfor
et urealisert potensiale i dataen som blir samlet.

\subsection{Oppgavebeskrivelse}
Vi skal levere en analyseplattform som kan rulles ut internt i en fabrikk
eller i skyen. Det skal i tillegg gjennomføres en generell undersøkelse av
fordeler og ulemper ved bruk av skytjenester, med spesielt fokus på faktorene
ytelse, kostnad og risiko. Gjennom analyseplattformen skal det være mulig å:

\begin{itemize}
	\item{Se løpende statistikk over nåværende produksjon i sanntid gjennom
	et dashbord. Det bør også være enkelt å tilpasse innholdet i dashbordet
	til ulike fabrikker.}
	\item{Generere skreddersydde rapporter med oversikt over en gitt
	tidsperiode. Rapportene bør også enkelt kunne skrives ut i papirformat.}
	\item{Se utfallene av en justering på klassifiseringsalgoritmen, ved at man
	øker eller senker toleransen for feil.}
\end{itemize}

\subsubsection{Avgrensning}
\begin{itemize}
	\item Analyseplattformen skal aggregere og analysere data fra produksjon,
	men eventuelle endringer som skal gjøres som følge av dette vil ikke
	gjøres gjennom vårt produkt.
	\item Vi vil levere et produkt som støtter internasjonalisering, men vi tar
	ikke ansvar for implementasjon av språk og enheter utover det som er oppgitt
	i kravspesifikasjon.
	\item Dashbordets grafiske utforming tar utgangspunkt i en større
	skjermflate, og vi vil ikke ta hensyn til synlighet på mobile enheter.
	\item Sluttproduktet skal være fungerende, men ikke nødvendigvis klart for produksjon.
\end{itemize}

\subsection{Prosjektmål}
\subsubsection{Resultatmål}

Prosjektet kan deles i fire hovedområder:

\begin{enumerate}
    \item Det skal være mulig å lage skreddersydde rapporter til hver enkelt fabrikk. 
    Rapportene må enkelt kunne skrives ut i PDF-format. 
    \item Det skal være mulig å se løpende oversikt over produksjonen
    innenfor angitte tidsområder.
    \item Det skal være mulig å simulere historisk produksjonsdata med forskjellige målekriterier. 
    \item Det skal gjøres en vurdering av fordeler og ulemper ved bruk av \linebreak
    skyløsninger. 
\end{enumerate}

Ved prosjektets slutt ønsker Argos at de fire punktene over er operative, slik
at det kan vises og eksperimenteres med. 

\subsubsection{Effektmål}
Argos har et ønske om at analyseplattformen skal bidra til å:
\begin{itemize}
    \item gi kunder enklere tilgang til produksjonsdata slik at feil oppdages
        raskere og svinn minimeres. Dette vil øke antall godkjente plater per
        skift.
    \item gi kundene økt utbytte av deres tjenester. Dette vil ha positiv
        effekt på selskapets fortjeneste og tilfredsstillelse blant kundene.
    \item redusere tid Argos bruker på vedlikehold og videreutvikling.
\end{itemize}

\subsubsection{Læringsmål}

Etter endt bachelor ønsker vi å ha mer kunnskap om:
\begin{itemize}
    \item verktøy som er aktuelle i arbeidslivet, slik at vi er mer effektive i daglig arbeid.
    \item å jobbe sammen i team, for å bli bedre forberedt til arbeidslivet.
    \item hvordan det er å jobbe med en smidig utviklingsmetode.
    \item dokumentasjon, mer spesifikt hvordan dokumentere slik at programvaren lett kan videreføres.
    \item testing og kvalitetskontroll 
        \begin{itemize}
            \item rutiner for testing og bruk av ulike verktøy.
            \item forstå viktigheten til testing.
            \item effekten av god kvalitetskontroll. 
        \end{itemize}
    \item hvordan utforme og skrive en informativ rapport. 
\end{itemize}

\subsection{Rammer}
\begin{itemize}
	\item Tidsrammen for prosjektet er satt ved leveringsfrist 20. mai 2021.
	\item Løsningen skal utvikles med forbehold om at en potensiell migrering
	til sky kan forekomme i fremtiden.  Dette innebærer at løsningen enkelt
	skal kunne migreres og kjøres i skyen dersom
	dette blir aktuelt. 
	\item Gruppen vil gjennom prosjektløpet måtte ta hensyn til to
	eksisterende teknologier og versjoner som foreligger av disse:
	\begin{itemize}
		\item{Windows Server 2019}
		\item{SQL Server 2012}
	\end{itemize}
\end{itemize}

\subsection{Organisering}
\subsubsection{Gruppemedlemmer}
Gruppen består av tre studenter ved NTNU i Gjøvik, hvor samtlige studerer
dataingeniør. Dette gjorde at vi hadde et nokså likt utgangspunkt med tanke på
faglig kompetanse, med unntak av forskjeller i valgemner.

Gjennom studieløpet har vi hatt flere emner som har gitt oss nyttige
kunnskaper i møte med bacheloroppgaven. Systemutvikling er et av de mer
fremtredende emnene, da planlegging, prosjektstyring og arbeidsmetodikk har
ringvirkninger gjennom alle prosjektets faser. Til tross for vår manglende
erfaring med prosjekter av denne størrelsesorden, har lærdom vi tilegnet
oss i dette emnet vært uvurderlig.

I forhold til valgemner har vi stor spredning i gruppen, noe vi anser som en
fordel. Den spredte kompetansen gjorde at vi kunne berike ulike deler av
prosjektet, som også er noe vi tok hensyn til i fordelingen av roller og
arbeidsoppgaver.

\subsubsection{Roller} Vi bestemte tidlig at det var hensiktsmessig å dele
oppgaven i ulike deler, og dermed delegere roller etter disse delene.  Etter
den initielle oppdelingen, ble det tidlig bestemt at dette skulle gjøres
basert på hvilken av delene som tiltalte vedkommende mest, og ikke
nødvendigvis der man hadde mest bakgrunnskunnskaper.  Til tross for at dette
tilsynelatende er et mindre effektivt alternativ, anså vi interesse og
læringsutbytte som viktigere momenter enn ren effektivitet.  I tillegg slo vi
tidlig fast at disse rollene ikke skulle være absolutte, men
heller fungere som et overordnet ansvarsområde.  I praksis betydde dette at
vi samarbeidet tett med alle deler av prosjektet, til tross for vår
delegering av roller.

\begin{itemize}
\item \textbf{Gruppeleder, simulering og infrastruktur} \\
Lars Stormark Pedersen hadde ansvar for infrastruktur og simuleringsdelen
av oppgaven.

I tillegg til å være utvikler, har Lars på bakgrunn av sin erfaring fra
arbeidslivet blitt utnevnt gruppeleder. Dette har innebært et overordnet
ansvar for fremdriften i prosjektet.

\item \textbf{Utvikler, dashbord og design} \\
Mads Olsen Nekkøy hadde hovedansvaret for dashbordet og alt
design-relatert.

\item \textbf{Utvikler, rapportgenerering og kvalitetsansvarlig} \\ 
Andreas Nygård Ljøterud var hovedansvarlig for rapportgenerering, og hadde
i tillegg hatt et overordnet ansvar for kvalitetsikring og dokumentasjon.

\item \textbf{Veileder} \\ 
Tom Røise, universitetslektor ved NTNU i Gjøvik.

\item \textbf{Oppdragsgiver} \\ 
Argos Solutions AS, representert ved Tor Nordseth, Anders Hørtvedt og Ole Martin
Ruud.
\end{itemize}

\begin{figure}[h!]  
    \centering
    \includegraphics[width=\textwidth]{org_chart.png} 
	\caption{Organisasjonskart.}
	\label{fig:org_chart}
\end{figure}

\subsection{Hvorfor valgte vi oppgaven}
Vi fikk høre om Argos og muligheten for et potensielt samarbeid gjennom vår
gode venn og tidligere medstudent Ole Martin Ruud, som nå er ansatt i
bedriften. Det ble tidlig i prosessen opprettet dialog, hvor vi ble presentert
med tre ulike oppgaver vi kunne velge mellom. Analyseplattformen vekket
umiddelbar interesse blant samtlige i gruppen. Vår ambisjon var at oppgavens
allsidighet ville introdusere faglige utfordringer på flere relevante
fagområder, noe som var svært tiltalende for oss.

\subsection{Målgrupper}
Prosjektet i sin helhet inneholder leveranser til flere parter som hver har
sine behov. Vi har derfor valgt å dele målgruppen i to deler: rapport og
analyseplattform.

\subsubsection{Rapport}
Målgruppen for rapporten er i første rekke sensor, som skal vurdere
prosjektet, og vår veileder, Tom Røise. På bakgrunn av dette er rapporten
skrevet med utgangspunkt i at leser har teknisk innsikt, men ikke
nødvendigvis kjennskap til spesifikke teknologier som er brukt.

\subsubsection{Analyseplattform}
For analyseplattformen i sin helhet, anser vi Argos og bedriftens kunder som
vår primære målgruppe. Vi har derfor gjennom hele prosjektløpet hatt tett
kommunikasjon med oppdragsgiver, for å best mulig forstå bedriftens og
sluttbrukernes ønsker og behov.


\subsection{Om rapporten}

\subsubsection{Navngivning}

Navnet \textit{Argos} kommer fra gresk mytologi, og blir gjerne brukt i
sammenheng med
 \textit{Sees med argosøyne}. Inspirert av dette, valgte vi å
kalle prosjektet
 \textit{Koios}. Koios er den greske guden for visdom og
nysjerrighet
 \cite{koios}. Navnet oversettes i
 moderne tid til querying,
questioning eller intelligence \cite{koiwik}, noe vi
 synes var passende med
tanke på bruksområdet til vårt system.

\subsubsection{Rapportens struktur}

Rapporten er strukturert sekvensielt på lignende måte som om prosjektarbeidet
hadde fulgt en fossefallsmodell. \\

\begin{itemize}
\item \hyperref[chapter:utviklingsprosess]{\textbf{Utviklingsprosessen}} redgjør
for hvordan gruppen har gått frem for å systematisere arbeidet.

\item \hyperref[chapter:kravspesifikasjon]{\textbf{Kravspesifikasjon}} beskriver
kravene som har blitt satt til systemet.

\item \hyperref[chapter:teknologier]{\textbf{Teknologier}} gir en oversikt
over hvilke teknologier som har blitt valgt.

\item \hyperref[chapter:design]{\textbf{Design}} definerer hvordan
systemet må utformes for å ivareta kravene. 

\item \hyperref[chapter:implementasjon]{\textbf{Implementasjon}} beskriver hvordan
design- og teknologivalgene har blitt realisert.

\item \hyperref[chapter:testing]{\textbf{Testing}} viser hvordan implementasjonen
har blitt kvalitetssikret.

\item \hyperref[chapter:installasjon]{\textbf{Installasjon}} omhandler relevante
faktorer for produksjonssetting.

\item \hyperref[chapter:sikkerhet]{\textbf{Sikkerhet}} redgjør hvordan systemet
ivaretar sikkerhetsutfordringer. 

\item \hyperref[chapter:drofting]{\textbf{Drøfting}} diskuterer
resultatet av prosjektet, og hvordan utviklingsprosessen har vært.

\item \hyperref[chapter:konklusjon]{\textbf{Konklusjon}} oppsummerer hele oppgaven og beskriver videre arbeid.
\end{itemize}
