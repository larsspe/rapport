\subsection{Rapportgenerering} 
Ettersom vi i rapportdelen endte opp med å lage
en \gls{proof_of_concept}, valgte vi å gjenskape en rapport fra Argos for å
vise potensialet i løsningen vår. Vi fikk tidlig i prosessen tilsendt
eksisterende rapporter som inspirasjon til hvilke data det var interessant å
vise, og vi valgte den mest brukte rapporten av disse som grunnlag (se
vedlegg~\ref{appendix:prodrep}). Vi anså det ikke som nødvendig å vise nøyaktig
de samme dataene i vår forbedret versjon, da formålet i all hovedsak dreiet seg
om å vise hva slags muligheter verktøyet kunne tilby. Den resulterende
konseptrealiseringen kan ses i vedlegg~\ref{appendix:prodrep_ny}.  For å vise
at dette valget ikke ble tatt på bakgrunn av begrensinger i vår løsning, har vi
i tillegg gjenskapt rapporten basert på data Argos syntes var viktigst.
Resultatet av gjenskapningen kan ses i vedlegg~\ref{appendix:prodrep_on}.

\subsubsection{Dataflyt}
For å illustrere dataflyten i rapportgenereringen, har vi valgt å følge et datasett fra konsept til realisering i rapport. Denne prosessen vil være identisk
for alle datasett som skal innføres, og består av følgende steg:

\begin{enumerate}
    \item[0.] Hvilke data vi er ute etter
    \item[1.] Henting av data
    \item[2.] Konvertering og registrering
    \item[3.] Definering av data
    \item[4.] Visualisering av data
\end{enumerate}

Datasettet vi skal se nærmere på i dette tilfellet er nedgraderinger per
defekttype (se figur \ref{fig:downgrades}), da dette er en av de mer sentrale
produksjonsmålene. Det er i flere av stegene inkludert deler av
spørringene for å beskrive det som diskuteres. Den fullstendige spørringen
finnes i vedlegg~\ref{appendix:downgrades_full}. 

\subsubsection*{\textbf{Steg 0:} Hvilke data vi er ute etter}
Før første steg i prosessen kan begynne, er det essensielt å vite hvilke data
som er nødvendig for gjennomføring. For å kreere grafen over er vi avhengig av
tre kolonner i databasen:

\begin{itemize}
    \item \textbf{Type}: Typen defekt.
    \item \textbf{Class}: Klassifisering av defekten. Klassifiseringen sier noe
    om hvilken klasse defekten tilhører. Er defekten av klasse 0 er den
    ubetydelig og trenger ingen ytterligere behandling. Klasse 1 og 2 vil
    henholdsvis si at defekten trenger reparasjon eller at den er så alvorlig at
    platen må vrakes. 
    \item \textbf{BoardID}: Den unike ID-en til hver plate, som her brukes for å telle antall forekomster av defekter. 
\end{itemize}

\subsubsection*{\textbf{Steg 1:} Henting av data}
Når vi har bestemt hvilke data vi er ute etter, er neste steg å hente
de fra databasen. 

\begin{lstlisting}[language=Python, caption={Spørring for å hente type, klasse og antallet unike forekomster.},
label={lstlisting:query}]
data = (
    session.query(Defects.c.Type, func.count(Boards.c.ID), Defects.c.Class)
    .join(Defects)
    .filter(Boards.c.Time.between(start_time, end_time))
    .filter(Defects.c.IsWorstDefect == "1")
)
\end{lstlisting}

I listing \ref{lstlisting:query} henter vi de tidligere nevnte dataene fra
databasen og filtrerer på start- og slutt-tidspunkt. Deretter filtrerer vi på om
defekten er markert som WorstDefect. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.4\textwidth]{worst_defect_exp.png}
    \caption{Hvordan WorstDefect status tildeles.}
    \label{fig:worst_defect_exp}
\end{figure}

Som navnet impliserer, forteller dette oss om feiltypen var grunnlaget for
nedgraderingen av platen. Som nevnt tidligere, og beskrevet i figur
\ref{fig:worst_defect_exp}, blir den feiltypen med høyest klasse for hver plate
markert som \lstinline{WorstDefect}. Grunnen til at vi her filtrerer på dette parameteret,
er at vi i vårt tilfelle kun er interessert i de feiltypene som faktisk
nedgraderte platene. 

\subsubsection*{\textbf{Steg 2:} Konvertering og registrering}
Ettersom vi i designfasen bestemte at Report Builder skulle aggregere
data fra rammeverket, måtte vi benytte XML som serialiseringsmetode. Dataene
som returneres av spørringen består av resulterende SQLAlchemy-rader. 

\vspace{0.5cm}
\begin{lstlisting}[language=Python, caption={Konvertering av data til XML.},
label={lstlisting:xml_conv}]
for item in dataset:
    # Set row child-node for each individual result in dataset
    row = ET.Element("Row")

for i, part in enumerate(item):
    # Set each part of every result as individual value child-nodes
    ET.SubElement(row, column_names[i]).text = str(part)

    yield row
\end{lstlisting}
\vspace{0.25cm}

I selve konverteringen \ref{lstlisting:xml_conv} aksesseres hvert individuelle
objekt i datasettet ved hjelp av dens tilhørende indeks, og for hvert av disse
genereres et
 XML-element. Resulterende trestruktur kan ses i listing
\ref{lstlisting:xml_structure}.

\vspace{0.5cm}
\begin{lstlisting}[language=XML, caption={Grunnleggende XML-struktur.},
label={lstlisting:xml_structure}]
<root>
    <Row>
        <DefectType></DefectType>
        <Count></Count>
        <Class></Class>
    </Row>
</root>
\end{lstlisting}
\vspace{0.5cm}

Selve behandlingen av dataen er nå ferdigstilt, og den er nå klar til og sendes
til Report Builder. Første steg i denne prosessen gjøres i listing
\ref{lstlisting:register}, der spørringen registreres som en modul. På denne
måten vil spørringen være tilgjengelig gjennom
\lstinline{/xml/query}-endepunktet, og kan dermed hentes gjennom Report Builder.\\

\begin{lstlisting}[language=Python, caption={Registrering av modul.},
    label={lstlisting:register}]
@plugmanager.register_plugin(
    "Downgrades", 
    category="report_builder"
)
def xml_get_downgrades(...)
\end{lstlisting}
\vspace{0.5cm}

\subsubsection*{\textbf{Steg 3:} Definering av data}
Det første som må gjøres for å få dataen inn i Report Builder er å legge inn
datakilden. For å oppnå denne tilkoblingen trenger vi kun å oppgi
tilkoblingsdetaljene, som består av IP, port og navnet på
endepunktet: \lstinline{http://localhost:8000/xml/query}. 

Når datakilden er definert vil neste steg bestå i å spesifisere hvilke
datasett\footnote{Datasett representerer et utvalg av data fra en datakilde.} vi er ute etter.
Defineringen av datasett foregår over to steg: \\

Først må det spesifiseres hvilke elementer i det innkommende datasettet vi er
interesserte i. Dette gjøres ved å opprette en spørring i \textit{XML
 Query
Syntax}, noe som syntaktisk vil gjøres på lignende måte som ved det
 mer kjente
Xpath \cite{xml_query_language}. Spørringen i listing~\ref{lstlisting:xml_query}
spesifiserer den såkalte \lstinline{ElementPathen}, eller elementstien, til dataene vi er
ute etter. Oppbyggingen av stien vil samsvare med den resulterende
trestrukturen fra steg to (se figur \ref{lstlisting:xml_structure}).

\begin{lstlisting}[language=Python, caption={XML Query Language spørring.},
    label={lstlisting:xml_query}]
<Query>
    <ElementPath>root/Row{DefectType, Class, Count}</ElementPath>
</Query>
\end{lstlisting}

Videre krever API-et fem faste parametere for å gjennomføre spørringer
mot databasen, og disse spesifiseres også i datasettet. Start- og sluttidspunkt
har vi valgt å definere som globale parametere for
rapporten, mens resterende verdier legges direkte i datasettet (se tabell
\ref{table:parameters}). Ved å definere globale parametere på denne måten, kan
brukeren selv spesifisere disse ved generering av rapporten. 

\begin{table}[]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        \rowcolor[HTML]{EFEFEF} 
        Parameter   & Parameterverdi                     \\ \hline
        start\_time & {\color[HTML]{6200C9} Brukerinput} \\ \hline
        end\_time   & {\color[HTML]{6200C9} Brukerinput} \\ \hline
        resolution  & 30 minutter                        \\ \hline
        targets     & scanner1                           \\ \hline
        action      & Downgrades                         \\ \hline
    \end{tabular}
    \caption{Parametere med tilhørende verdier.}
    \label{table:parameters}
\end{table}

\subsubsection*{\textbf{Steg 4:} Visualisering av data}
Ved ferdigstilt oppsett av datasettet (se figur~\ref{fig:datasett}), gjenstår
bare visualisering i rapport. I Report Builder gjøres dette simpelten ved å
velge diagramtype og deretter plassere data inn i diagrammet. I vårt tilfelle
har vi, for dette datasettet, valgt å representere dataen gjennom et liggende
stolpediagram (se figur~\ref{fig:downgrades}). 

\begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{datasett.png}
    \caption{Datasett i Report Builder.}
    \label{fig:datasett}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{downgrades.png}
    \caption{Antall nedgraderinger per defekttype.}
    \label{fig:downgrades}
\end{figure}

\subsection{Gruppering av innhold}
I den opprinnelige produksjonsrapporten (se vedlegg~\ref{appendix:prodrep}) er
innholdet gruppert, noe vi anså som en sentral del av eksisterende
funksjonalitet. Dette
 valgte vi å implementere i vår løsning ved å implementere
to ytterligere
 datasett parametere \lstinline{gbcolumn_name} og
\lstinline{gbcolumn_value}.
 Disse beskriver henholdsvis navnet på kolonnen i
databasen det skal sorteres på
 og verdien til denne kolonnen. 

Ettersom enhver rapport ikke nødvendigvis skal grupperes, valgte vi å
implementere disse som valgfrie. Dette var enkelt å implementere, da det gjennom
API-et allerede var lagt til rette for ved at tilleggsinformasjon til spørringene
kunne innføres gjennom \lstinline{extra_data}. På denne måten
kunne vi supplere spørringene med ytterligere data slik at resultatet ble
gruppert. Dette ble implementert ved å filtrere dataen før den sendes til
endepunktet, dersom ytterligere parametere er oppgitt (se listing
\ref{lstlisting:gbcolumn}).

\begin{lstlisting}[language=Python, caption={Gruppering i spørring.},
label={lstlisting:gbcolumn}]
# If group-by column is included in call, filter data by it
if "gbcolumn_name" in extra_data:
    data = data.filter(
        getattr(Boards.c, extra_data["gbcolumn_name"])
        == extra_data["gbcolumn_value"]
    )
\end{lstlisting}

I Report Builder legges det til rette for tilsvarende funksjonalitet gjennom
subreports. En subreport fungerer som en referanse til en annen frittstående
rapport, og på den måten er det mulig å generere den basert på andre parametere
i hovedrapporten. Dermed kunne vi supplere subreporten med hvilket parameter den
skulle grupperes på, og videre hente data basert på dette parameteret gjennom \lstinline{gb_column}-parametrene.
