I dette kapittelet viser vi hvordan teknologivalgene, kravene og designet har
blitt brukt for å implementere de funksjonalitetene som er ønsket. Kapittelet er
delt inn i tilsvarende måter som tidligere, der vi først viser hvordan
fellesplattformen har blitt utviklet, for så å beskrive hvordan de eksterne
tjenestene benytter seg av denne.



\subsection{Rammeverk for aksessering av data}

Rammeverket for aksessering av data er bindeleddet mellom de eksterne
tjenestene og databasene til Argos. Plattformen er bygd for å være fleksibel,
men samtidig oppfylle alle kravene som satt i kravspesifikasjon og design.
Rammeverket implementerer FastAPI for å ta imot spørringer, og sender dette
videre til en egenlaget modulkontroller som finner modulen med ønsket data.
Disse modulene er skreddersydde, og det er lagt opp til at det er enkelt for
både Argos' ansatte og deres kunder utvide systemet med ekstra moduler dersom
det er ønsket. Modulene får automatisk tilgang til relevante SQLAlchemy
sesjoner, slik at det er enkelt å aksessere data fra nåværende databaseløsning.

\subsubsection{Rammeverkets grunnstruktur}

Listing~\ref{listing:tree} viser hvordan rammeverkets grunnstruktur er bygd som
en tradisjonell Python pakke, der hvert lag er implementert i sin egen fil.
Dette medfører at det er mulig å kjøre applikasjonen som en helhet, men det er også mulig
å bare kjøre utvalgte deler av dette dersom det er ønskelig. 

\begin{lstlisting}[caption={ArgosAnalysePlatform filstruktur.}, label={listing:tree}]
ArgosAnalysePlatform
    html
    plugins
        __init__.py
        boards.py
        defects.py
        heatmap.py
        regfile_reader.py
        xml_converter.py
    __init__.py
    __main__.py
    api.py
    database.py
    plugin_manager.py
    util.py
\end{lstlisting}

Dataflyten når spørringer gjennomføres kan sees i figur~\ref{fig:pluginmanager_dataflow}.

\subsubsection{API}

API-laget har ansvar for eksponering av tilgjengelige endepunkter, samt
validering av innkommende og utgående datastrukturer. Dette gjorde vi ved å
benytte oss av FastAPI\@.  Kjernen til FastAPI består av å \gls{dekorere} Python
funksjoner som mottaker av \lstinline{GET}, \lstinline{POST}, \lstinline{PUT} eller
\lstinline{DELETE} operasjoner.  For å kunne skalere ved større systemer er det i tillegg
mulig å benytte FastAPI \lstinline{Router} klasser, som deretter kan
importeres av kjerneklassen for å få tilgang til de aktuelle endepunktene.
Figur~\ref{figure:fastapi_distribution} viser hvordan vi har implementert denne
fordelingen.

Basert på designet og kravene til API-et, kom vi frem til at de fleste
spørringene er avhengige av tilgang til databasen. For å gi denne tilgangen
benyttet vi oss av FastAPI sitt innebygde system for å dynamisk legge ved
ekstra data i innkommende spørringer.  I listing~\ref{listing:query_definition}
blir \lstinline{/query} endepunktet satt opp gjennom \lstinline{api.py} ruteren
fra figur~\ref{figure:fastapi_distribution}.  Dette gjorde det enkelt å legge
ved databasen til bruk i de forskjellige endepunktene. Denne modellen medfører
at tilgangen til databasene bare spesifiseres én gang, og gjør det
enkelt å bytte ut denne ved testing. \\

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{fastapi_oppsett.png}
    \caption{Fordeling over FastAPI ruter, endepunkter og avhengigheter.}
    \label{figure:fastapi_distribution}
\end{figure}

\begin{lstlisting}[
    language=Python, 
]
from ArgosAnalysePlatform.api import router as api_router

class APIApp(FastAPI):
    def __init__(self):
        self.include_router(api_router)

\end{lstlisting}
\begin{lstlisting}[
    language=Python, 
    caption={Forkortet utdrag fra analyseplattformen.},
    label={listing:query_definition},
    firstnumber=6
]
router = APIRouter()

class JSONQueryInputType(BaseModel):
    range: TimeRangeType
    intervalMs: int
    scopedVars: Dict[str, ScopedVarsModel]    
    targets: List[TargetsModel]

@router.post(
    "/query", 
    response_model=Union[JSONQueryTimeseriesResponseModel, (...)]
)
def query(request: Request, inp: JSONQueryInputType):
    ...
\end{lstlisting}

En sentral del av API-laget, som nevnt i design
(\ref{section:teknologi_api_rammeverk}), er valideringen av innkommende og
utgående datastruktur.  FastAPI benytter seg av Pydantic for serialisering og
deserialisering av data. Pydantic kombinerer Python sitt innebygde typesystem med
egne klasser for mer komplekse modeller. Disse klassene kan arve av hverandre
for å lage ytterligere komplekse strukturer. Dersom Pydantic får felter den
ikke kjenner igjen, vil den enten ignorere disse eller avbryte med en
feilmelding. I listing~\ref{listing:query_definition} defineres inputmodellen
ved \lstinline{inp: JSONQueryInputType} og output gjennom
\lstinline{response_model=}. Fullt kodeeksempel av dette utdraget kan finnes i
vedlegg~\ref{appendix:query_full_code_example}.

\subsubsection{Modulkontrolleren}

Modulkontrollerens ansvar er å holde en oversikt over systemets registrerte
moduler, og gi et grensesnitt for kall til bruk i API-et. Vi har
implementert dette ved hjelp av en egenskrevet klasse. For å registrere en
modul kreves navnet som skal brukes til oppslag, samt hvilken kategori modulen
skal høre til. Dette kan ses i første del av
listing~\ref{listing:pluginmanager}. Når moduler har blitt registrert er det
også mulig å søke blant modulene dersom man ikke vet det nøyaktige navnet og
kategorien for modulen man ønsker.

For å kalle modulene vil modulkontrolleren returnere det registrerte
funksjonsobjektet, basert på oppgitt navn og kategori. Dette
funksjonsobjektet kan deretter benyttes som en ellers ordinær Python funksjon.
Denne implementasjonen av mikrokjernearkitekturen gir en fordel ved at det er
mulig å gruppere modulene basert på hvilken ekstern tjeneste de er ment å
benyttes med. Disse faktorene mitigerer også vanskeligheten med grensesnittet
mellom kjernen og modulene, som er en svakhet med mikrokjernemodellen.
Modulkontrolleren i seg selv trenger ikke å vite noe om hvilke parametre som
blir benyttet med det returnerte funksjonsobjektet.  Et eksempel på dette kan
sees i andre halvdel av listing~\ref{listing:pluginmanager}.\\

\begin{lstlisting}[language=Python]
plugmanager = PluginManager()

@plugmanager.register_plugin(
    'plugin_name', 
    category='report_builder'
)
def plugin_callable(txt: str):
    print(txt)
\end{lstlisting}
\begin{lstlisting}[
    language=Python,
    caption=Registrering og kall av modul,
    label=listing:pluginmanager,
    firstnumber=9
]
# Kall modul gjennom modulkontrolleren.
pluginmanager_returned_callable = plugmanager.call_plugin(
    'plugin_name', 
    category='report_builder'
)
pluginmanager_returned_callable('sample text')

# Kall modul som en vanlig funksjon.
plugin_callable('Sample text')
\end{lstlisting}

\subsubsection{Håndtering av databasetilkoblinger}

I kravspesifikasjonen og designet ble det avdekket at det var ønskelig å enkelt
kunne benytte seg av data fra flere databaser. Dette gjorde at
databasetilgangslaget måtte ta hånd om kommunikasjon med enkeltdatabasene, samt
tilby et grensesnitt for hvordan spørringene kunne gjøres mot flere databaser av gangen.
Dette har vi implementert ved at SQLAlchemy tar seg av spørringer til
enkeltdatabasene, mens en egenskrevet klasse gir et grensesnitt for hvordan
disse spørringene kjøres mot flere databaser.

For å kunne gjøre spørringer med SQLAlchemy må det oppgis hvor databasen er.
Dette gjør SQLAlchemy i sitt \lstinline{engine} objekt. Dette objektet trenger
informasjon om vertsnavn, brukernavn, passord, port og databasenavn for å koble
seg til. For å sikre brukervennligheten og vedlikeholdbarheten til systemet var
det viktig at det er enkelt for brukerene å legge til eller endre disse
parameterene. Vi valgte å lagre denne dataen i en YAML-fil, da dette formatet
er mer brukervennlig enn JSON\@ for konfigurasjon~\cite{yaml_vs_json}.
Strukturen av denne filen kan sees i listing~\ref{listing:connections_yaml}.
Her lagres parameterene med et egendefinert nøkkelord som navn, og nødvendige
detaljer som innhold.\\


\begin{lstlisting}[
    caption={Spesifisering av tilkoblingsdetaljer for database i YAML.},
    label={listing:connections_yaml}
]
shared:
    dbname: DatabaseName
    port: 1433

connections:
    wood_grain:
        ip: 1.1.1.1
        password: pass
    wood_smooth:
        ip: 2.2.2.2
        password: pass2
\end{lstlisting}



\subsubsection{Håndtering av databaseobjekter}

Når tilkoblingsdetaljene er spesifisert benyttes de for å lage
\lstinline{Sessionmaker}-objekter. Disse objektene benyttes for å generere
sesjoner, som brukes av modulene for å gjøre spørringene mot databasen. De
nødvendige stegene for å sette opp en enkelt tilkobling kan sees i
listing~\ref{listing:sqla_setup}.\\

\begin{lstlisting}[language=Python, caption=Minimal kode for å gjøre spørringer
med SQLAlchemy., label={listing:sqla_setup}]
engine = create_engine('mssql+pyodbc://username:password@ip/tablename')
sess_maker = sessionmaker(bind=engine)
session = sess_maker()
return session.query(...)
\end{lstlisting}


Når flere databaser blir spesifisert kan det bli komplisert å holde styr på
disse individuelle objektene. Dette løste vi ved hjelp av en egenlaget klasse
som tok hånd om lesing av tilkoblingsdetaljer, og oppsett av \lstinline{Engine}-
og \lstinline{Sessionmaker}-objektene til bruk.\\

Modulene vet hvilke databaser de ønsker data fra, og trenger et
sesjonsobjekt for hver av disse databasene for å gjøre spørringene. Med klassen
vi laget for å håndtere databasetilkoblingene, var det enkelt å legge til
rette for dette. Listing~\ref{listing:sessioniter} viser hvordan samme spørring
kjøres mot flere databaser samtidig gjennom vårt grensesnitt.\\

\begin{lstlisting}[language=Python, caption=Bruk av egenskrevet databaseklasse., label={listing:sessioniter}]
db = DatabaseConnections(...)
for name, session in db.sessioniter(['wood_grain', 'wood_smooth']):
    return session.query(...)
\end{lstlisting}

\subsubsection{Knytte alt sammen}

Endepunktene \lstinline{/query} og \lstinline{/xml/query} knytter alle disse lagene
sammen for å gi de eksterne systemene tilgang til etterspurt data fra
databasen. Begge disse endepunktene benytter seg av dataflyten som vist i
sekvensdiagrammet i figur~\ref{fig:pluginmanager_dataflow}.

Endepunktene tar de samme parametrene: start- og sluttidspunkt,
tidsbolkoppløsning, ønskede skannere og hvilke moduler som skal kalles. Endepunktene
prosesserer disse parametrene, før den benytter seg av databasekontrolleren for å 
finne sesjonsobjekter, og modulkontrolleren for å finne modulene. Listing~\ref{listing:shortened_query}
en forkortet versjon av hvordan dette er implementert i \lstinline{/query} endepunktet.\\

\begin{lstlisting}[language=Python, caption={Sammenkobling av lagene i \lstinline{/query}.}, label={listing:shortened_query}]
@router.post("/query")
def query(request: Request):
    # Forbered tidsparametre her.
    scanners = ...
    modules = ...

    response = []
    for module_name in modules
        for db_name, session in request.database.sessioniter(scanners):
            response.extend(
                plugmanager.call_plugin(module_name, category="")(
                    session,
                    db_name,
                    ...
                )
            )

    return response
\end{lstlisting}

\subsubsection{Oppstart}

Før oppstart av API-et, trenger den informasjon om miljøet den skal kjøre i. Dette
angis gjennom spesifiserte kommandolinjeargumenter som tolkes av Click~\cite{click}.
Dette er et rammeverk som gjør det enkelt å spesifisere og validere
kommandolinjeargumenter i Python. Click validerer typene til parametrene, samt
legger til rette for en beskrivende hjelpetekst via \lstinline{--help}, som
vist i listing~\ref{listing:helptext}.\\

\begin{lstlisting}[caption={\lstinline{--help} for rammeverket.},
label={listing:helptext}] Usage: __main__.py [OPTIONS]
  Run the Argos Analysis platform.

  Will launch the API with specified options.

Options:
  --external-plugins TEXT  Path to external plugins
  --debug                  Enable debug mode. This will show 
                           extra debug output and enable 
                           automatic reload when source files 
                           change.

  --no-reflect             Do not reflect the database tables
                           if set.

  --port INTEGER           The port to run the server on
  --host TEXT              The host to listen on
  --help                   Show this message and exit.
\end{lstlisting}


Flyten i programmet varierer basert på om brukeren ønsker å kjøre FastAPI i sin
\lstinline{--debug} modus eller ikke. Dersom debug modus er satt, vil FastAPI
starte API-tjeneren i en annen kontekst enn den som fikk
kommandolinjeargumentene.  For å komme oss rundt dette ble parametrene gitt til
hovedinstansen lagret i JSON\@, som leses i den nye Python konteksten.
Figur~\ref{fig:kommandolinje_flyt} viser et flytskjema av denne prosessen.

\begin{figure}
    \includegraphics[width=\textwidth]{kommandolinje_flytskjema.png}
    \caption{Prosessering av kommandolinjeargumenter~\label{fig:kommandolinje_flyt}.}
\end{figure}
