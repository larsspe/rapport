.PHONY: clean watch all

all: main

main:
	latexmk -pdf main

clean:
	rm chapters/*.aux main.aux main.bcf main.out main.run.xml

watch:
	inotify-hookable  \
		-f chapters/1_introduction.tex \
		-f chapters/2_utviklingsprosess.tex \
		-f chapters/3_kravspesifikasjon_analyse.tex \
		-f chapters/4_teknologier_api.tex \
		-f chapters/5_design_analyse.tex \
		-f chapters/5_design_api.tex \
		-f chapters/6_implementasjon_analyse.tex \
		-f chapters/6_implementasjon_api.tex \
		-f chapters/7_testing.tex \
		-f chapters/8_installasjon.tex \
		-f main.tex \
		-c "make all" \
