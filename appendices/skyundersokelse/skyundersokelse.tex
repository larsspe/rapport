\documentclass[12pt]{article}
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage[norsk]{babel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{color,soul}
\usepackage[labelfont=bf]{caption}
\graphicspath{ {../../images/} }

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    bookmarks=true,
    pdfpagemode=FullScreen,
    citecolor=magenta
}

\title{Hvordan påvirker faktorene ytelse, risiko og kostnad en migrering fra lokal infrastruktur til sky?}
\date{}
\bibliography{references}

\begin{document}
\maketitle
\clearpage

\section{Introduksjon}

Det er ønskelig at analyseplattformen skal fungere både i skyen og internt i
fabrikken. I den forbindelse ønsker Argos å sammenligne lokale og
skybaserte løsninger, med fokus på risiko, ytelse og kostnad. Det er et spesielt
fokus på sikkerhet, grunnet skepsis fra Argos sine kunder om å sende data ut
i skyen.

\subsection{Problemstilling}

I denne vurderingen skal vi besvare følgende spørsmål: \\\\
Hvordan påvirker faktorene ytelse, risiko og kostnad en migrering fra lokal infrastruktur til sky?

\subsection{Metode}

Innhenting av informasjon og kunnskap skjedde fortrinnsvis gjennom
litteratursøk. Hovedressursene vi tok i bruk var akademiske rapporter og
statlige utredninger.

\subsection{Avgrensning}

Skytjenester er et stort fagfelt og vi har begrenset med tid. På bakgrunn av
dette må vi definere et sett med avgrensninger.

\begin{itemize}
    \item Begrense antall tekniske detaljer, da oppdragsgiver ønsker en
    overordnet vurdering i motsetning til en teknisk detaljert rapport.
    \item Vurderingen er del av en større oppgave, gruppen må derfor
    prioritere tidsbruken. Det ble bestemt at hovedinnsatsen i oppgaven
    skulle gå over en 2 ukers periode.
\end{itemize}

\subsection{Hva er skytjenester?}
\label{hva_er_sky}
Det finnes ikke en entydig definisjon på hva skytjenester er, eller et sett
med standarder en tjeneste må oppfylle for å bli kalt en
skytjeneste \cite{ffi}. Regjeringens definisjon av skytjenester tar
utgangspunkt i den amerikanske standardiseringsorganisasjonen
National Institute of Standards and Technology (NIST).
NIST definerer skytjenester ved å bestemme fem essensielle
egenskaper en tjeneste må inneha for å kunne kalles en skytjeneste \cite{nist}:

\begin{enumerate}
    \item \textbf{Selvbetjent oppsett} \\
    Kunder kan administrere ressurser ved behov, uten noen form for
    menneskelig interaksjon med tjenesteleverandør.

    \item \textbf{Tilgang via nettverk} \\
    Tjenestene er tilgjengelig over nettverk gjennom standardmekanismer fra
    ulike typer klienter.
    
    \item \textbf{Samling av ressurser} \\
    Leverandørens ressurser er samlet slik at de dynamisk kan fordeles etter
    behov.

    \item \textbf{Umiddelbar fleksibilitet} \\
    Ressursene bak en skytjeneste er fleksible i det at de raskt kan skaleres
    opp eller ned etter behov.

    \item \textbf{Ressursbruk måles} \\
    Ressursbruk styres og optimaliseres ved hjelp av automatiske måleverktøy.
    Oversikt over bruk og kostnader er dermed lett tilgjengelig for både kunde og leverandør.

\end{enumerate}

\subsubsection*{Tjenestemodeller}
\label{tmod}
Skytjenester deles opp i tjenestemodeller, som hver sikter på å dekke
virksomheters ulike behov. Tjenestemodellene beskriver ansvarsfordelingen
mellom skyleverandør og kunde. De tre vanligste er: Programvare som
tjeneste, plattform som tjeneste og infrastruktur som
tjeneste.

\begin{itemize}

\item \textbf{Programvare som tjeneste} (Software-as-a-Service,
SaaS) overlater mest ansvar til skyleverandøren. Kunden benytter
simpelthen leverandørens applikasjoner gjennom for eksempel nettleser eller
annen tynn klient. Med SaaS administrerer kunden verken underliggende
infrastruktur, servere, operativsystemer eller lagringsmuligheter.

Eksempler på SaaS er Google Workspace og Microsoft Office 365. 

\item \textbf{Plattform som tjeneste} (Platform-as-a-service,
PaaS) gir kunden tilgang til en forvaltet plattform for bygging og
levering av digitale tjenester i skyen. Kunden kan innføre egne
applikasjoner og benytte disse verktøyene i skyen, i stedet for å selv
måtte investere i infrastruktur. I likhet med SaaS har leverandør
ansvar for drift og vedlikehold, men kunden har kontroll over egne
applikasjoner.

Eksempler på PaaS er AWS Lambda og Red Hat OpenShift. 

\item \textbf{Infrastruktur som tjeneste}
(Infrastructure-as-a-Service, IaaS) gjelder umiddelbar levering
av datainfrastruktur, klargjort og administrert over internett. Med andre
ord er leverandøren ansvarlig for infrastruktur, mens kunden benytter seg
av leverandørens virtualiserte ressurser. Dette medfører at kunden selv
administrerer operativsystemer, lagringsmuligheter og distribuerte
applikasjoner.

Eksempler på Iaas er Microsoft Azure og Linode. 

\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{sky_modeller.JPG}
    \caption{Hver av tjenestemodellenes anvarsfordeling sammenlignet med
    lokal infrastruktur \cite{NSM}.}
\end{figure}

\subsubsection*{Leveransemodeller}
Skytjenester kan leveres på flere måter: 

\begin{itemize}
    \item \textbf{Offentlig tilgjengelig sky} (public cloud) er
    skytjenester som er åpent tilgjengelige for allmenheten.
    
    \item \textbf{Privat tilgjengelig sky} (private cloud) er en
    lukket skytjeneste som kun er tilgjengelig for den virksomheten tjenesten
    skal gjelde for.
    
    \item \textbf{Hybridsky} (hybrid cloud) er en blanding mellom
    offentlig og privat sky.
    
    \item \textbf{Gruppesky} (community cloud), hvor flere
    virksomheter deler en privat sky.
\end{itemize}

\section{Diskusjon}
I følge en undersøkelse gjort i 2018 av Vanson Bourne på vegne av
Teradata \cite{teradata}, er flertallet av de største teknologiselskapene i
verden enige om at skyen er det beste stedet å kjøre analyser. Samme
undersøkelse viser også at noen av de største barrierene for å flytte
analyser til skyen er bekymringer knyttet til sikkerhet, ytelse, overholdelse
av regelverk, integrasjon med eldre systemer, mangel på kompetanse og mangel
på tillit. 

\subsection{Ytelse} 
Undersøkelser viser at en av de mer fremtredene bekymringene blant bedrifter
er lav ytelse i skyen \cite{teradata}. Faktorer som kan påvirke ytelse er
blant annet geografisk plassering mellom fabrikk og datasenter, bedriftenes
nettverksstruktur og systemets optimalisering \cite{apccp}.

De største skyleverandørene har serverparker over hele verden, og som kunde
har man mulighet for å bestemme geografisk plassering av
data \cite{azure},
nettopp for å imøtekomme disse utfordringene. Dette kan føre til ekstra
kostnader, men samtidig forenkle juridiske utfordringer knyttet til
overføring av data på tvers av landegrenser \cite{cc:oac}.

Selv om skyløsninger reduserer avhengigheten av lokal infrastruktur, er det
essensielt at det eksisterer pålitelig nettverksstruktur. Systemer setter
 ulike krav, da ytelsen for enkelte tjenester er direkte knyttet til
overføringshastighet. Eksempelvis er det helt avgjørende for et
overvåkingssystem at data som blir representert ikke er utdatert. Geografisk
plassering av fabrikken kan påvirke kostnaden ved
 utvidelse av kapasitet. 

En av de største fordelene med skytjenester er den dynamiske tilgangen på
ressurser. Den umiddelbare tilgangen til skalert prosessorkraft og lagring
gir stor fleksibilitet. Tjenester som er optimalisert for skyen vil yte bedre
og dermed koste mindre, da man kun betaler for de ressursene man bruker. En
løsning som ikke er utviklet for skyen, vil ikke ha spesielt utbytte av
fordelene en skytjeneste tilbyr. Det er derfor viktig at utviklere planlegger
spesifikt for ytelse i skyen \cite{apccp}. \\ Det er viktig å nevne at man bør
være oppmerksom på vendor lock-in \cite{vendorlock}. For høyest mulig fleksibilitet bør ikke applikasjonen være optimalisert for én spesifikk skytjeneste, men skyen
generelt. \\

Utdraget fra Nasjonal strategi for bruk av skytenester,
viser nytteverdien av dynamisk ressurstildeling \cite{nstrat}: \\

\begin{quote} 
\textit{I desember 2010 flytta BaneDanmark informasjonsnettsida si til ei
skyteneste (Microsoft Azure). Om vinteren blei det store problem med
transporttenestene i Danmark. Dei andre transportselskapa opplevde at
informasjonstenestene svikta på grunn av stor pågang frå publikum. Dette
skjedde ikkje med Banedanmark. På det meste hadde dei 5,5 millionar brukarar
på ein dag, mot normalt 50 000. Den auka kapasiteten dei trong i denne
perioden betalte dei 179 DKK for.\\}
\end{quote}

\subsection{Kostnad}
Stadig flere bedrifter benytter seg i dag av
skytjenester \cite{ssb}.
Eksempelvis har regjeringen i Storbritannia innført preferansepolitikk på
skytjenester grunnet ønsket om økt standardisering og redusering av
kostnader \cite{govuk}. Skytjenester reduserer ikke bare
kostnaden, men kan også øke bedriftens fokus på egen virksomhet og legge til
rette for nyskapning. Lavere risiko og mindre behov for nødvendig
startkapital kan medføre økt tålmodighet ved innovasjon i
organisasjonen. Et eksempel er Telenor sin Comoyo satsing \cite{nstrat}: \\

\begin{quote}
    \textit{Comoyo var Telenor si satsing på strøyme-TV. Tenesta blei
    etablert allereie i 2011. Så seint som i mai 2013 uttalte Telenor: «Med
    nyetablerte Comoyo skal Telenor kapre 130 millionar forbrukarar i alle
    kanalar og på alle plattformer.» Telenor avvikla Comoyo i november 2013,
    etter at store internasjonale aktørar som Netflix og HBO etablerte seg
    med strøyme-TV i Norden og tok det meste av marknaden. Telenor brukte
    Amazon sin infrastruktur til å levere tenesta på. Dermed betalte dei
    berre for den kapasiteten dei trong for å betene dei kundane dei hadde
    til kvar tid. Då tenesta blei lagt ned, satt dei derfor ikkje igjen med
    store investeringar i infrastruktur dei ikkje lenger hadde bruk for.}
\end{quote}

Lokale tjenester har den ulempen at skalering må skje gjennom nye
investeringer i hardware og software. En skytjeneste krever derimot ingen
investering utover allokering av nye ressurser. I tillegg til kostnader ved
skalering, må en lokal server driftes. Det innebærer ressurser til oppsett,
vedlikehold, oppgraderinger og overvåkning. En skyløsning vil også redusere
eller eliminere behovet for å drifte en lokal løsning, og dermed redusere
antall årsverk. \\

Både lokal- og skyløsninger krever infrastruktur i henhold til nettverk, men
hvilken som er mest kostnadseffektiv krever en dypere
granskning. \\ Løsninger med komplisert arkitektur og tett integrasjon med
eksisterende systemer kan gjøre en skyløsning mindre kostnadseffektiv sammenlignet med en lokal løsning. En applikasjon som er designet for bruk i skyen vil ha bedre ytelse og gi lavere kostnader, men arkitekturen og designet
av tjenesten er avgjørende. \\ Ved at forbrukere samler ressursene på ett sted,
er energieffektiviteten ved bruk av skytjenester høyere. Dette er mer
effektivt kontra at hver kunde har sine egne datasentre \cite{nstrat}. 

\subsection{Sikkerhet}

\textit{“Bruk av skytjenester er kommet for å bli, men det er viktig å ikke
se seg blind på lavere kostnader alene.”} sier NSM i sitt helhetlige
digitale risikobilde for 2020 \cite{NSM}. Den tidligere nevnte undersøkelsen
av Vanson Bourne \cite{teradata}, viser at nærmest halvparten av de 700 store
teknologiselskapene som ble intervjuet var bekymret for sikkerheten i skyen.
NSM stiller seg positive til at virksomheter tar i bruk skytjenester, så
lenge det gjennomføres gode vurderinger i forkant \cite{NSM}.

Regjeringen ønsker at IKT-virksomhet skal gjøres sikrere og mer
kostnadseffektiv, og vil derfor gjøre det lettere for virksomheter å vurdere
skytjenester som et alternativ. I nasjonal strategi for bruk av
skytjenester har de derfor gjort rede for noen viktige vurderinger som bør
gjøres før en anskaffer skytjenester. Disse rådene gir virksomheter et
overblikk over de viktigste faktorene for å benytte skytjenester på
forsvarlig vis \cite{nstrat}. Nedenfor er punktene gruppa anså som
sikkerhetsrelevante beskrevet og koblet opp mot relevant tematikk: \\

\begin{itemize}
\item \textbf{Sourcing} dreier seg om hvilke valg en virksomhet foretar seg
når de skal vurdere hvilke tjenester de skal håndtere eller drifte selv, og
hvilke ansvar som skal delegeres eksterne aktører.

Kjøp av skytjenester vil være et slikt valg, da skyleverandørene vil stå
ansvarlig for varierende deler av driften ut fra valg av tjenestemodell
(\ref{tmod}). Da skyleverandørene gjerne satser tungt på sikkerhet \cite{ffi},
anser NSM dette aspektet ved skytjenester som fordelaktig, spesielt for
mindre bedrifter \cite{NSM}. Dette på bakgrunn av at bedriftene gjerne ikke
har kapasitet eller kompetanse til å utføre nødvendig sikkerhetsarbeid. \\

\item \textbf{Informasjonssikkerhet} er et sett med prinsipper utrettet for å
håndtere risiko relatert til data innad i en virksomhet. Nedenfor er disse
prinsippene kort oppsummert og diskutert: \\

\textbf{Konfidensialitet} - forhindre at data aksesseres av uvedkommende. 

Risikoen knyttet til oppbevaring av data i skyen, varierer med hvor sensitiv
dataen er \cite{nstrat}. Dersom det er snakk om personopplysninger eller annen
sensitiv informasjon, må bedrifter forholde seg til et strengere regelverk
\cite{pvern}.
Regjeringen anbefaler at kunder gjennomfører en grundig kontroll av
leverandør før en eventuell anskaffelse \cite{nstrat}.

Mange virksomheter stiller seg skeptisk til å legge data i skyen, da de føler
på tap av kontroll når data håndteres av en tredjepart. Regjeringen har utarbeidet ulike
mekanismer for å redusere denne frykten \cite{nstrat}. FFI fant i sin undersøkelse i 2018
ingenting som tydet på at datasentere som leverer skytjenester er mer sårbare
for angrep enn tradisjonelle datasentere \cite{ffi}. Dette står i tråd med NSM
sin erfaring om at hovedandelen uønskede sikkerhetsrelaterte hendelser var
grunnet konfigurasjon- eller brukerfeil, ikke sårbarheter hos
leverandør \cite{NSM}. I likhet med andre områder innen informasjonssikkerhet,
har også skyleverandører egne
sertifiseringer\footnote{https://cloudsecurityalliance.org/star/}. I tillegg
til spesialiserte driftsavdelinger, har skyleverandørene fysiske sikringer
ved sine serverparker med strenge restriksjoner. Disse parkene er ofte
sertifisert basert på nivået av sikkerhet ved
anleggene\footnote{https://datasenter.basefarm.no/datasenter/sikkerhet/}. \\

\textbf{Integritet} - forhindre at data ikke kan endres av uvedkommende. 

Overordnet har vi ikke funnet noe konkret som tilsier at det er store
forskjeller mellom skytjenester og lokal infrastruktur som påvirker
dataintegritet. Uansett hvilken modell man bruker, er det viktig å følge
gjeldende retningslinjer for å ivareta integritet. \\

\textbf{Tilgjengelighet} - data skal være tilgjengelig ved behov. 

Som nevnt i \ref{hva_er_sky}, er en essensiell karakteristikk ved
skytjenester at ressursene er fleksible, og at de enkelt kan skaleres både
opp og ned. Denne fleksibiliteten skaper redundans ved at dersom en server
skulle bli utilgjengelig, kan tilsvarende ressurser spinnes opp umiddelbart
for å dekke behovet til feilen er fikset. Denne egenskapen er svært
fordelaktig, spesielt hvis dataene anses som kritiske, og vil være
utfordrende å replikere ved bruk av lokal infrastruktur. Skytjenester er
lokasjonsuavhengige, som gjør de helt avhengig av internett for å kunne
aksesseres (\ref{hva_er_sky}). En virksomhets nettverkstilkobling er derfor å
anse som en sårbarhet ved bruk av skytjenester \cite{ffi}.
\end{itemize}

\section{Konklusjon}

Initielt i undersøkelsen stilte vi følgende spørsmål: \\\\ 

Hvordan påvirker faktorene ytelse, risiko og kostnad en migrering fra lokal
infrastruktur til sky?

For å svare på dette har vi først introdusert hva skytjenester er, og deretter
undersøkt hvordan de utvalgte faktorene påvirker en potensiell migrering.

En av de største fordelene med skytjenester er den dynamiske tilgangen på
ressurser. For å utnytte denne fleksibilitene til det fulle, må systemene
optimaliseres spesifikt til skytjenester. Dette kan medføre risiko for
ytterligere kostnader og vendor lock-in. Skytjenester er helt avhengige av
internett, og det stilles derfor større krav til pålitelig eksternt nettverk
enn ved lokal infrastruktur.

En migrering til sky anses i all hovedsak som en fordelaktig beslutning med
tanke på risiko. Den dynamiske tilgangen til ressurser skaper en redundans
som er vanskelig å replikere ved bruk av lokal infrastruktur. Leverandørene
har spesialisert kompetanse innen sikkerhet og drift, og er derav ofte bedre
egnet til å gjennomføre sikkerhetsarbeid enn bedriften selv. For å unngå
bekymringer knyttet til tap av kontroll er tillit til leverandørene
essensielt.

En annen styrke med skytjenester er at man betaler for nøyaktig det man
trenger, ved at ressurser enkelt kan allokeres og frigjøres. Lokale tjenester
tilbyr ikke samme fleksibilitet, da de krever investering i infrastruktur ved
skalering. Denne fleksibiliteten gir bedrifter frihet til å fokusere på
egen virksomhet og kan legge til rette for nyskapning. \\

Basert på våre undersøkelser, støtter faktorene ytelse, risiko og kostnad
oppunder en potensiell migrering til skyen. På generelt grunnlag vil vi
anbefale Argos å ta i bruk skytjenester, med forbehold om at faktorene bør
vurderes ytterligere ved hvert migreringstilfelle.

\clearpage
\printbibliography

\end{document}
